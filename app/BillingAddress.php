<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class BillingAddress extends Model
{
    static public function insertBilling($data){
        BillingAddress::insert(BillingAddress::billingAttrs($data));
    }

    static public function updateBilling($data){
        BillingAddress::where('user_id', Auth::user()->id)->update(BillingAddress::billingAttrs($data));
    }

    static public function billingAttrs($data){
        return [
            'address' => $data->address1.' '.$data->address2,
            'city' => $data->city,
            'state' => $data->state,
            'postal_code' => $data->postal_code,
            'country' => $data->country,
            'user_id' => Auth::user()->id,
            'created_at' => new \DateTime('NOW')
        ];
    }
}
