<?php

namespace App;
use App\Expertise;
use App\Inbox;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $fillable = [
        'expertise_id ', 'expertise_user_id', 'user_id','status'
    ];
    public function expertise()
    {
        return $this->belongsTo(Expertise::class);
    }

    public function messages()
    {
        return $this->hasMany(Inbox::class);
    }
}
