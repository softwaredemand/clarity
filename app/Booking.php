<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CallRequest;

class Booking extends Model
{
    public function call_request()
    {
        return $this->belongsTo(CallRequest::class);
    }
}
