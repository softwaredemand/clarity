<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Booking;

class CallRequest extends Model
{
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }
}
