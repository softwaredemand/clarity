<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Answer;
use App\QuestionTopic;

class Question extends Model
{
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function question_topics()
    {
        return $this->hasMany(QuestionTopic::class);
    }
}
