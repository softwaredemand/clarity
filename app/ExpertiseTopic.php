<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpertiseTopic extends Model
{
    //
     protected $fillable = [
        'topic_id', 'expertise_id'
    ];


 	// public function topic()
  //   {
  //       return $this->hasMany('App\TicketConversation');
  //   }

    public $timestamps = false;

}
