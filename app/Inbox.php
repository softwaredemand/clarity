<?php

namespace App;
use App\Conversation;
use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    protected $table = 'inbox';

    public function conversation()
    {
        return $this->belongsTo(Conversation::class);
    }
}
