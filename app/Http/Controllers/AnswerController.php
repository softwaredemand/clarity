<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use App\QuestionTopic;
use App\RecentActivity;
use App\User;
use Illuminate\Http\Request;

class AnswerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only(['create']);
    }

    public function create($questionId){
        $question = Question::find($questionId);
        return view('pages.answers.create', compact('question'));   
    }

    public function store(Request $request){

        $request->validate([
            'body' => 'required',
        ]);
        Answer::insert([
            'body' => $request->input('body'),
            'question_id' => $request->input('question_id'),
            'user_id' => $request->input('user_id'),
            'status' => 1,
            'created_at' => new \DateTime('NOW')
        ]);
        $question = Question::find($request->input('question_id'));
        $user = User::find($request->input('user_id'));
        $body = [
            'question' => $user['name'].' answered "'.$question['body'].'"',
            'answer' => $request->input('body'),
        ];
        RecentActivity::insert([
            'activity_type' => 'answer',
            'body'=> json_encode(array_filter($body)),
            'user_id' => $user['id'],
            'created_at' => new \DateTime('NOW')
        ]);

        return response()->json([
            'message' => "Your Answer Added!"
        ], 200);
    }

    public function show($question_id = null)
    {
        $question = Question::where('id', $question_id)->with('answers','answers.user')->first();
        $questions = Question::select('id','body')->with('answers','answers.user')->get();
        $question_topics = QuestionTopic::where('question_id',$question['id'])->get();
        $question_topics = $question_topics->load('topic')->groupBy('question_id');
        return view('pages.answer_details', compact('question','questions','question_topics'));   
    }  
}
