<?php

namespace App\Http\Controllers;

use App\Review;
use DateTime;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input('expertise_id'))
            $reviews = Review::select('body','user_id','reviews.created_at as created_at','users.name as user_name','users.profile_image as profile_image')
                ->where([
                'review_type' => 'expertise',
                'review_id' => $request->input('expertise_id')
                ])->join('users','users.id','reviews.review_id')->orderBy('reviews.created_at','DESC')->get();
        else
            $reviews = Review::select('body','user_id','reviews.created_at as created_at','users.name as user_name','users.profile_image as profile_image')
                    ->where([
                    'review_type' => 'expert',
                    'review_id' => $request->input('user_id')
                    ])->join('users','users.id','reviews.review_id')->orderBy('reviews.created_at','DESC')->get();

        foreach($reviews as $row)
        {
            $row->profile_image = $row->profile_image ? asset('storage/images/profile/'.$row->profile_image) : 'images/img9.png';
        }

        return response()->json([
            'reviews' => $reviews
        ], 200);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Review::insert([
            'body' => $request->input('body'),
            'review_type' => $request->input('expertise_id') ? "expertise" : "expert",
            'review_id' => $request->input('expertise_id') ? $request->input('expertise_id') : $request->input('user_id'), //user_id is the expert id which is actually user
            'user_id' => $request->input('auth_user') ? $request->input('user_id') : $request->input('user_id'),
            'created_at' => new \DateTime('NOW')
        ]);

        return response()->json([
            'message' => "Your Review Added!"
        ], 200);
    }
}
