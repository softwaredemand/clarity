<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Category;
use App\Expertise;
use App\ExpertiseTopic;
use App\Image;
use App\BillingAddress;
use App\RecentActivity;
use Auth;
use Illuminate\Support\Facades\Session;
use Storage;
use App\Favourite;
use App\Wallet;
use Stripe;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['update_info','update_image','apply_for_expert','allExpertise']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $User = User::find(Auth::user()->id);
        $User->short_name = $User->short_name();
        
        $User->profile_image = $User->profile_image ? asset('storage/images/profile/'.$User->profile_image) : 'images/thumb.jpg';

        return view('pages.account',compact('User'));   
    }

    public function profile()
    {
        $User = User::find(Auth::user()->id);
        
        return view('pages.profile',compact('User'));   
    }

    public function info($anexpert = null)
    {
        $User = User::find(Auth::user()->id);

        return view('pages.info',compact('User','anexpert'));   
    }

    public function image()
    {
        $User = User::find(Auth::user()->id);

        return view('pages.image', compact('User'));   
    }

    public function Verifications()
    {
        return view('pages.Verifications');   
    }

    public function UserProfileView($username = null)
    {   
        if($username)
            $user = User::where('username', $username)->first();
        else
            $user = User::find(Auth::user()->id);

        if($user)
            $user->short_name = $user->short_name();
        else
            return redirect()->back();
        $user->profile_image = $user->profile_image ? asset('storage/images/profile/'.$user->profile_image) : 'images/thumb.jpg';
        
        $experts_fvrt = Favourite::where('favourable_type','expert')->where('favourable_id', $user->id)->get();
        $user['favroite_count'] = count($experts_fvrt);
        $expertise_fvrt_user_ids = $experts_fvrt->pluck('user_id');

        $related_experts = User::select('id','name','username','short_bio','location','mini_resume','profile_image','hourly_rate')->where('id', '!=', Auth::user()->id)->get();

        foreach($related_experts as $row)
        {
            $row->profile_image = $row->profile_image ? asset('storage/images/profile/'.$row->profile_image) : 'images/thumb.jpg';
        }

        return view('pages.userprofileview', compact('user','expertise_fvrt_user_ids','related_experts'));   
    }

    public function dashboard()
    {
        $recent_activities = RecentActivity::select('activity_type','body','recent_activities.created_at','users.profile_image as avatar')
        ->join('users','users.id','recent_activities.user_id')
        ->orderBy('recent_activities.created_at','DESC')->get();
        return view('pages.dashboard', compact('recent_activities'));   
    }

    public function notification()
    {
        return view('pages.notification');   
    }
    public function promotion()
    {
        return view('pages.promotion');   
    }
    public function changePassword()
    {
        $user_id = Auth::user()->id;
        return view('pages.change_password', compact('user_id'));   
    }
    public function earned()
    {
        $earnings = Wallet::where('user_id',Auth::user()->id)->sum('amount');
        return view('pages.earned', compact('earnings'));
    }
    public function creditCard()
    {
        $card = null;
        $stripe = new \Stripe\StripeClient(
            'sk_test_51J9dJDBmJaUTgpglFqkre7dugUZuQdE357wBI4jvupD7GLHWL6DqyjprzPz9EUaGEkK5JlkHa3xdUbftUqlPeDWy00WkXn9PqF'
        );
        $user = Auth::user();
        if($user->customer_id)
            $card = $stripe->customers->retrieveSource(
                $user->customer_id,
                $user->stripe_card_info,
                []
            );
        return view('pages.credit_card', compact('card'));   
    }
    public function billingAddress()
    {
        $billing = BillingAddress::where('user_id', Auth::user()->id)->first();
        return view('pages.billing_address', compact('billing'));
    }

    public function storeBillingInfo(Request $request)
    {
        $billing = BillingAddress::where('user_id', Auth::user()->id)->first();
        if(!$billing)
            BillingAddress::insertBilling($request);
        else
            BillingAddress::updateBilling($request);

        return redirect()->back();
    }
    
    public function update_info(Request $request)
    {

        $response = ["status" => 'Success', "message" => 'Profile info updated successfully'];

        $User = User::where('username', '=', $request->info['username'])->where('id', '<>',$request->info['id'])->first();

        if (!empty($User)) {
            $response = ["status" => 'Error', "message" => 'This Username already exist..'];
        } else 
        {
            User::where('id', '=', $request->info['id'])->update($request->info);
        }

        Session::flash('profileToast', true);
        
        return response()->json(['response' => $response]);
    }   

    public function update_image(Request $request)
    {   
        $filename = substr($request->profileImage['file'], strpos($request->profileImage['file'], ',') + 1);
            
        $name = rand(1111, 9999) . time();

        $name .= '.' . 'jpg';   

        Storage::put('images/profile/'. $name, base64_decode($filename));

        User::where('id', '=', $request->user_id)->update(['profile_image' => $name]);
    }

    

    public function stripePost(Request $request)
    {
        $stripe = new \Stripe\StripeClient('sk_test_51J9dJDBmJaUTgpglFqkre7dugUZuQdE357wBI4jvupD7GLHWL6DqyjprzPz9EUaGEkK5JlkHa3xdUbftUqlPeDWy00WkXn9PqF');
        $user = Auth::user();
        if ($user->customer_id)
            $old_customer = $stripe->customers->retrieve($user->customer_id,[]);
        else
        {
            $customer = $stripe->customers->create([
                'email' => $user->email,
            ]);
            $card = $stripe->customers->createSource(
                $customer['id'],
                ['source' => $request->stripeToken]
            );
            $user->customer_id = $customer['id'];
            $user->stripe_card_info = $card['id'];
            $user->save();
        }
          
        return back();
    }

    // public function apply_for_expert(Request $request)
    // {   
    //     $response = ["status" => 'Success'];
     
    //     User::where('id', '=', $request->user_id)->update(['is_expert' => $request->is_expert]);

    //     return response()->json(['response' => $response]);


    // }
}
