<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Expertise;
use App\Booking;
use App\CallRequest;
use App\BillingAddress;
use Auth;

class BookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['booking','store']);
    }
    public function booking(Request $request)
    {
        $source = $request->source;
        $user = User::where('username', $source)->first();
        if(!$user)
        {
            $expertise = Expertise::find($source);
            $user = User::find($expertise->user_id);
        }        
        $user->profile_image = $user->profileImage();
        $billing = BillingAddress::where('user_id', Auth::user()->id)->first();
        return view('pages.booking', compact('user','source','billing'));   
    }

    public function store(Request $request)
    {
        $user_id = is_numeric($request->source) ? Expertise::where('id', $request->source)->pluck('user_id') : User::where('username', $request->source)->pluck('id');
        $call_request_id = CallRequest::insertGetId([
            'user_id' => Auth::user()->id,
            'source_id' => $request->source,
            'source_type' => is_numeric($request->source) ? 'Expertise' : 'Expert',
            'status' => 0,
            'created_at' => new \DateTime('NOW')
        ]);
        
        Booking::insert([
            'message' => $request->message,
            'estimated_length' => $request->approximate_length,
            'suggest_datetime' => json_encode(array_filter($request->date)),
            'call_request_id' => $call_request_id,
            'user_id' => $user_id[0],//receiver_id
            'booker_id' => Auth::user()->id,//receiver_id
            'created_at' => new \DateTime('NOW')
        ]);

        if($request->cc || $request->cvv || $request->cc_month || $request->cc_year)
        {
            $stripe = new \Stripe\StripeClient('sk_test_51J9dJDBmJaUTgpglFqkre7dugUZuQdE357wBI4jvupD7GLHWL6DqyjprzPz9EUaGEkK5JlkHa3xdUbftUqlPeDWy00WkXn9PqF');
            $user = Auth::user();
            if ($user->customer_id)
                $old_customer = $stripe->customers->retrieve($user->customer_id,[]);
            else
            {
                $customer = $stripe->customers->create([
                    'email' => $user->email,
                ]);
                $card = $stripe->customers->createSource(
                    $customer['id'],
                    ['source' => 'tok_amex']
                );
                $user->customer_id = $customer['id'];
                $user->stripe_card_info = $card['id'];
                $user->save();
            }
        }
        
        $billing = BillingAddress::where('user_id', Auth::user()->id)->first();
        if(!$billing)
            BillingAddress::insertBilling($request);
        else
            BillingAddress::updateBilling($request);
        
        return redirect('/');
    }

    public function calls()
    {
        $calls = Auth::user()->load(['bookings','bookings.call_request']);

        $users = User::all();
        return view('pages.calls', compact('calls','users'));   
    }
}
