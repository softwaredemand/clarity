<?php

namespace App\Http\Controllers;

use App\RecentActivity;
use Illuminate\Http\Request;

class RecentActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RecentActivity  $recentActivity
     * @return \Illuminate\Http\Response
     */
    public function show(RecentActivity $recentActivity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RecentActivity  $recentActivity
     * @return \Illuminate\Http\Response
     */
    public function edit(RecentActivity $recentActivity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RecentActivity  $recentActivity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RecentActivity $recentActivity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RecentActivity  $recentActivity
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecentActivity $recentActivity)
    {
        //
    }
}
