<?php

namespace App\Http\Controllers;

use App\Favourite;
use App\Expertise;
use App\Image;
use App\User;
use App\RecentActivity;
use Helper;
use Illuminate\Http\Request;

class FavouriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = $expertise_images = array();

        $expertise_ids = Favourite::where([
            'favourable_type' => 'expertise',
            'user_id' => $request->input('user_id')
            ])->pluck('favourable_id');

        $query = Expertise::select('expertises.id as expertise_id','expertises.created_at as created_at','users.profile_image','users.hourly_rate','users.location','users.name as user_name','users.id as user_id','title','description','categories.name as category','categories.key as category_key') 
            ->join('categories','categories.id','expertises.category_id')
            ->join('users','users.id','expertises.user_id')
            ->whereIn('expertises.id', $expertise_ids)
            ->get();

        $expertise_images = Helper::expertiseImages();

        $expert_ids = Favourite::where([
            'favourable_type' => 'expert',
            'user_id' => $request->input('user_id')
            ])->pluck('favourable_id');
        
        $experts = User::select('id','name','username','short_bio','location','mini_resume','profile_image','hourly_rate')->whereIn('id',$expert_ids)->get();
        
        foreach($experts as $row)
        {
            $row->profile_image = $row->profile_image ? asset('storage/images/profile/'.$row->profile_image) : 'images/thumb.jpg';
        }

        return response()->json([
            'expertise' => $query,
            'experts'=> $experts,
            'expertise_images' => $expertise_images
        ], 200);
        
    }

    public function favorites()
    {
        return view('pages.favorites');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = $request->input('auth_user') ? $request->input('auth_user') : $request->input('user_id');
        $favourable_type = $request->input('expertise_id') ? "expertise" : "expert";
        $favourable_id = $request->input('expertise_id') ? $request->input('expertise_id') : $request->input('user_id');
        
        $Favourite = Favourite::where(['user_id' => $user_id, 'favourable_id' => $favourable_id])->first();
        
        if (empty($Favourite)) 
        {
             Favourite::insert([
                'favourable_type' => $favourable_type,
                'favourable_id' => $favourable_id,
                'user_id' => $user_id
            ]);
           
            $message = array('message' => "Added in your favourites");
        }
        else{
            Favourite::where(['user_id' => $user_id, 'favourable_id' => $favourable_id])->first()->delete();
            $message = array('message' => "remove from in your favourites");
        }
       
        if($favourable_type == "expertise")
        {   
            $expertise = Expertise::find($favourable_id);
            $user = User::find($expertise['user_id']);
            $body = $user['name'].' - ('.$expertise['title'].') was favorited';
        }
        elseif($favourable_type == "expert")
        {
            $expert = User::find($favourable_id);
            $body = $expert['short_bio'] ? $expert['name'].' - ('.$expert['short_bio'].') was favorited' : $expert['name'].'was favorited';
        }
            
        RecentActivity::insert([
            'activity_type' => $favourable_type,
            'body' => $body,
            'user_id' => $user_id,
            'created_at' => new \DateTime('NOW')
        ]);

        return response()->json($message, 200);
    }
}
