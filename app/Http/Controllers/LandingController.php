<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Helper;
use App\Category;
use App\User;
use App\Expertise;
use App\LandingInfo;
use App\Question;

class LandingController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['booking']);
    }

    public function index()
    {
        $landing_data = LandingInfo::latest('id')->first();
        return view('pages.landing', compact('landing_data'));   
    }

    public function search(Request $request)
    {
        $terms = explode(" ", $request->input('query'));
        
        $search_results = Expertise::select('expertises.id as expertise_id','expertises.created_at as created_at','users.profile_image','users.hourly_rate','users.location','users.name as user_name','users.id as user_id','title','description','categories.name as category','categories.key as category_key') 
            ->join('categories','categories.id','expertises.category_id')
            ->join('users','users.id','expertises.user_id')
            ->where(function ($query) use ($terms) {
                foreach ($terms as $term) {
                    $query->orWhere('title', 'like', '%' . $term . '%');
                    $query->orWhere('description', 'like', '%' . $term . '%');
                }
            })->get();

        $expertise_images = Helper::expertiseImages();

        return view('pages.search', compact('search_results','expertise_images'));
    }

    public function browse()
    {
        return view('pages.browse');   
    }
    
    public function browse_category($category)
    {
        $category = Category::select('name')->where('key', $category)->get()->first();
        return view('pages.browse_category', compact('category'));   
    }
    
    public function qustionNew()
    {
        return view('pages.qustion_new');   
    }

    public function customer()
    {
        return view('pages.customer');   
    }

    public function help_articles()
    {
        return view('pages.help_articles');   
    }
    
    public function help()
    {
        return view('pages.help');   
    }
    
    public function how_it_work()
    {
        return view('pages.how_it_work');   
    }
    
    public function terms()
    {
        return view('pages.terms');   
    } 

    public function customer_name()
    {
        return view('pages.customer_name');   
    } 

    public function getCategories(){
        return response()->json([
            'categories' => Helper::categories()
        ], 200);
    }
}
