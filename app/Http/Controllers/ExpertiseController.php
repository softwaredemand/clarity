<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expertise;
use App\User;
use App\Category;
use App\ExpertiseTopic;
use App\Topic;
use App\Image;
use Auth;
use Illuminate\Support\Facades\Session;
use Storage;
use App\Favourite;
use Helper;
use App\Review;

class ExpertiseController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth')->only(['expertiseOpen']);
    }

    public function expertise()
    {
        $user_id = Auth::user()->id;
        return view('pages.expertise', compact('user_id'));   
    }

    public function expertise_info( $anexpert = null )
    {
        $User = User::find(Auth::user()->id);

        return view('pages.expertise_info', compact('User','anexpert'));   
    }

    public function expertise_video()
    {
        return view('pages.expertise_video');   
    }

    public function expertise_add($createEdit, $anexpert = null)
    {
        $User = User::find(Auth::user()->id);

        $Category = Category::get();
        
        if ($createEdit == 'edit') {

            $expertise = Expertise::select('expertises.id as expertise_id','title','description','categories.id as category_id','categories.name as category_name') 
            ->join('categories','categories.id','expertises.category_id')
            ->where('expertises.id',$anexpert)
            ->first();
            
            $expertise_images = Image::where('imageable_type', 'expertise')->where('imageable_id', $expertise['expertise_id'])->get();

            $expertise_topic_ids = ExpertiseTopic::where('expertise_id', $expertise['expertise_id'])->pluck('topic_id');
            $topics = Topic::select('id','key','name')->whereIn('id',$expertise_topic_ids)->get();

            foreach($expertise_images as $row)
            {
                $row->image = asset('storage/images/expertise/'.$row['image']);
            }
            
            return view('pages.expertise_add', compact('User','Category','anexpert','expertise', 'expertise_images','topics'));
        }
        return view('pages.expertise_add', compact('User','Category','anexpert'));   
    }

    public function expertise_update(Request $request)
    {   
        
        if ($request->image) 
        {
            $filename = substr($request->image['file'], strpos($request->image['file'], ',') + 1);
            $name = rand(1111, 9999) . time();

            $name .= '.' . 'jpg';   

            Storage::put('images/expertise/'. $name, base64_decode($filename));
        }
        
        if($request->expertise_id)
        {
            $Expertise = Expertise::find($request->expertise_id);
            $Expertise->user_id = $request->user;
            $Expertise->title = $request->expertise['title'];
            $Expertise->description = $request->expertise['description'];
            $Expertise->category_id = $request->expertise['category'];
            $Expertise->status = 'active';
            $Expertise->save();
        }
        else
            $Expertise = Expertise::create([
                'user_id' => $request->user,
                'title' => $request->expertise['title'],
                'description' => $request->expertise['description'],
                'category_id' => $request->expertise['category'],
                'status' => 'active'
            ]);   
        
        if(isset($name))
            Image::insert([
                'imageable_id' => $Expertise->id,
                'imageable_type' => 'expertise',
                'image' => $name
            ]);

        $topic_ids = array();
        foreach($request->topics as $t){
            $topic_id = Topic::where('name',$t)->pluck('id')->first();
            array_push($topic_ids, $topic_id);
        }
        ExpertiseTopic::where('expertise_id',$Expertise->id)->delete(); //delete all old expertise topics
        foreach($topic_ids as $t_id){
            ExpertiseTopic::insert([
                'expertise_id' => $Expertise->id,
                'topic_id' => $t_id
            ]);
        }
        
        return response()->json([
            'message' => "Saved!"
        ], 200);

    }

    public function hourly_update(Request $request)
    {   
        $hourly = [
            'hourly_rate' => $request->hourly_rate['hourly'],
            'charity'     => $request->hourly_rate['charity'],
        ];

        if (isset($request->anexpert)) 
            $hourly['is_expert'] = true;
    
        User::where('id', '=', $request->user)->update($hourly);

        return response()->json([
            'message' => "Updated"
        ], 200);
    }

    public function allExpertise(Request $request, $category = null){
        
        if($category)
            $query = Expertise::select('expertises.id as expertise_id','expertises.created_at as created_at','users.profile_image','users.hourly_rate','users.location','users.name as user_name','users.id as user_id','title','description','categories.name as category','categories.key as category_key') 
                ->join('categories','categories.id','expertises.category_id')
                ->join('users','users.id','expertises.user_id')
                ->where('categories.name',$category)
                ->inRandomOrder()  
                ->get();
        elseif($request->input('user_id'))
            $query = Expertise::select('expertises.id as expertise_id','expertises.created_at as created_at','users.profile_image','users.hourly_rate','users.location','users.name as user_name','users.id as user_id','title','description','categories.name as category','categories.key as category_key') 
                ->join('categories','categories.id','expertises.category_id')
                ->join('users','users.id','expertises.user_id')
                ->where('expertises.user_id',$request->input('user_id'))
                ->inRandomOrder()  
                ->get();
        else
            $query = Expertise::select('expertises.id as expertise_id','expertises.created_at as created_at','users.profile_image','users.hourly_rate','users.location','users.name as user_name','users.id as user_id','title','description','categories.name as category','categories.key as category_key') 
                ->join('categories','categories.id','expertises.category_id')
                ->join('users','users.id','expertises.user_id')
                ->inRandomOrder()  
                ->get();

        $expertise_images = Helper::expertiseImages();

        foreach($query as $row)
        {
            $row->profile_image = asset('storage/images/profile/'.$row->profile_image);
        }

        return response()->json([
            'expertise' => $query,
            'expertise_images' => $expertise_images
        ], 200);

    }

    public function expertiseOpen($userid, $expertiseid)
    {
        $user = User::find($userid);
        $user->profile_image = $user->profile_image ? asset('storage/images/profile/'.$user->profile_image) : 'images/img1.png';

        $expertise = Expertise::find($expertiseid);
        $category = Category::find($expertise->category_id);

        $expertise_images = Image::select('imageable_id', 'image')->where('imageable_type', 'expertise')->where('imageable_id', $expertiseid)->get();
        $expertise_topics = Topic::select('name')
            ->join('expertise_topics','expertise_topics.topic_id', 'topics.id')
            ->where('expertise_id', $expertiseid)
            ->pluck('name');

        $expertise_fvrt = Favourite::where('favourable_type','expertise')->where('favourable_id',$expertiseid)->get();
        $expertise["favroite_count"] = count($expertise_fvrt);
        $expertise_fvrt_user_ids = $expertise_fvrt->pluck('user_id');
        return view('pages.expertise_open',compact('user','expertise','category','expertise_images','expertise_topics','expertise_fvrt_user_ids'));
    }   

}
