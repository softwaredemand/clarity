<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topic;
use App\UserInterestedTopic;
use Auth;

class TopicController extends Controller
{   
    public function topics()
    {
        return view('pages.topics');   
    }  

    public function topicname()
    {
        return view('pages.topicname');   
    }  

    public function topic()
    {
        $userId = Auth::user()->id;
        return view('pages.topic',compact('userId'));
    }

    public function saveTopic(Request $request)
    {
        $topics = $request->name;

        foreach($topics as $t){
            Topic::insert([
                'key' => strtolower(preg_replace('/\s*/', '', $t)),
                'name' => $t
            ]);
        }
        
        return response()->json([
            'message' => 'Topic added'
        ], 200);

    }

    public function saveUserInterestedTopic(Request $request)
    {
        $topics = $request->input('name');
        $topic_ids = array();
        foreach($topics as $t){
            $topic_id = Topic::where('name',$t)->pluck('id')->first();
            array_push($topic_ids, $topic_id);
        }
        UserInterestedTopic::where('user_id',$request->input('user_id'))->delete(); //delete all old user topics
        foreach($topic_ids as $t_id){
            UserInterestedTopic::insert([
                'user_id' => $request->input('user_id'),
                'topic_id' => $t_id
            ]);
        }
        
        return response()->json([
            'message' => 'Your Interest updated'
        ], 200);

    }

    public function allTopics(Request $request)
    {
        $topics = Topic::select('id','name','key')->get();
        
        return response()->json([
            'topics' => $topics
        ], 200);

    }

    public function userTopics(Request $request)
    {
        $topic_ids = UserInterestedTopic::where('user_id', $request->input('user_id'))->pluck('topic_id');
        $topics = Topic::select('id','name','key')->whereIn('id', $topic_ids)->get();
        return response()->json([
            'topics' => $topics
        ], 200);

    }
}
