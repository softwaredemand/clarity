<?php

namespace App\Http\Controllers;

use App\Conversation;
use App\Expertise;
use App\Inbox;
use App\User;
use DateTime;
use Illuminate\Http\Request;
use Helper;
use App\Events\MessageSent;

class ConversationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['index','create']);
    }

    public function index()
    {
        return view('pages.conversations.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $sourceId = null)
    {
        $conversation = 0;
        if (\Route::current()->getName() == 'conversation') {
            $conversation = Conversation::find($sourceId);
            $expertise = Expertise::find($conversation['expertise_id']);
        }
        else
            $expertise = Expertise::find($sourceId);
        
        return view('pages.conversations.create', compact('expertise','conversation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $expertise = Expertise::find($request->input('expertise_id'));

        if($request->input('conversation_id'))
            $conversation = Conversation::where('expertise_id', $request->input('expertise_id'))
            ->where('id', $request->input('conversation_id'))
            ->where('expertise_user_id', $expertise['user_id'])
            ->first();
        else
            $conversation = Conversation::where('expertise_id', $request->input('expertise_id'))
            ->where('user_id', $request->input('user_id'))
            ->first();
        
        if(!$conversation)
        {
            $conversation_id = Conversation::insertGetId([
                'expertise_id' => $request->input('expertise_id'),
                'user_id' => $request->input('user_id'),
                'expertise_user_id' => $expertise['user_id'],
                'status'=> 0,
                'created_at' => new \DateTime('NOW')
            ]);
        }
        else
        {
            $conversation_id = $conversation['id'];
            $conversation->update(['status' => 0]);
        }

        Inbox::insert([
            'body' => $request->input('body'),
            'user_id' => $request->input('user_id'),
            'conversation_id' => $conversation_id,
            'created_at' => new \DateTime('NOW')
        ]);
        
        $user = User::find($request->input('user_id'));
        
        // broadcast(new MessageSent($user->profileImage, $request->input('body')))->toOthers();

        return response()->json([
            'message' => "Message sent!"
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Conversation  $conversation
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {        
        if($request->input('conversation_id'))
            $conversation_id = Conversation::where('expertise_id', $request->input('expertise_id'))
                ->where('id', $request->input('conversation_id'))
                ->pluck('id');
        else
            $conversation_id = Conversation::where('expertise_id', $request->input('expertise_id'))
                ->where('user_id', $request->input('user_id'))
                ->pluck('id');

        $messages = Inbox::select('body','inbox.created_at as created_at','users.profile_image as profile_image')
        ->join('users','users.id','inbox.user_id')
        ->whereIn('conversation_id',$conversation_id)
        ->get();
        
        $messages = Helper::makeUserProfileUrl($messages);

        return response()->json([
            'messages' => $messages
        ], 200);
    }

    public function authUserInbox(Request $request){
        $user = User::find($request->input('user_id'));
        $messages = $user->load(['expertises', 'expertises.conversations','expertises.conversations.messages']);
        $messages = $messages->makeHidden($this->skipColumns());

        $conversations = $user->load(['conversations','conversations.messages']);
    
        $users = User::select('id','name','profile_image','username')->get();
        $users = Helper::makeUserProfileUrl($users);

        return response()->json([
            'messages' => $messages,
            'conversations' => $conversations,
            'users' => $users
        ], 200);
    }

    public function skipColumns(){
        return [
            'cell_code',
            'cell_phone',
            'email_verified_at',
            'is_expert',
            'mini_resume',
            'location',
            'id',
            'short_bio',
            'time_zone',
            'charity',
            'updated_at',
        ];
    }
}
