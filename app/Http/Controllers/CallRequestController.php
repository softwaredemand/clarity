<?php

namespace App\Http\Controllers;

use App\CallRequest;
use App\User;
use App\Wallet;
use Mail;
use Illuminate\Http\Request;
use Auth;

class CallRequestController extends Controller
{
    public function updateStatus(Request $request){
        $call_request = CallRequest::find($request->call_request_id);
        $call_request->status = $request->status;
        $call_request->save();
        
        if($request->status == 1)
        {
            $seller_will_recieve = 0;
            $customer = User::find($request->booker_id);
            $seller = User::find($request->sender_id);

            if($request->estimated_length == 15)
                $seller_will_recieve = ($customer->hourly_rate / 4);
            elseif($request->estimated_length == 30)
                $seller_will_recieve = ($customer->hourly_rate / 2);
            elseif($request->estimated_length == 60)
                $seller_will_recieve = $customer->hourly_rate;

            //charge customer
            $stripe = new \Stripe\StripeClient('sk_test_51J9dJDBmJaUTgpglFqkre7dugUZuQdE357wBI4jvupD7GLHWL6DqyjprzPz9EUaGEkK5JlkHa3xdUbftUqlPeDWy00WkXn9PqF');
            $transaction_done = $stripe->customers->createBalanceTransaction(
                $customer->customer_id,
                ['amount' => $seller_will_recieve, 'currency' => 'usd']
            );
            //charge customer

            //send money to wallet of seller
            if($transaction_done){
                Wallet::insert([
                    'type' => 'usd',
                    'amount' => $seller_will_recieve,
                    'user_id' => $seller->id,
                    'created_at' => new \DateTime('NOW')
                ]);
            }

            $arr = array('call' => "https://meet.jit.si/".rand(1111,9999), 'time' => $request->datetime);
            
            Mail::send('mail', $arr, function ($message) use ($customer,$seller) {
                $message->to($seller->email, $seller->name)
                    ->subject('You have scheduled a call request with '.$customer->name. '.');
                    $message->from($customer->email, $customer->name);
            });

            Mail::send('mail', $arr, function ($message) use ($customer,$seller) {
                $message->to($customer->email, $customer->name)
                    ->subject($seller->name. ' invited you for meeting');
                    $message->from($seller->email, $seller->name);
            });
        }

        return response()->json([
            'message' => 'Status Updated'
        ], 200);
    }
}
