<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['deleteUser']);
    }

    public function setting()
    {
        return view('pages.setting');   
    }
    public function deleteUser(Request $request)
    {
        User::where('id', '=', $request->user_id)->update(['is_deleted' => true]);
       
        $response = ["status" => 'Success', "message" => 'Account has been deleted'];
        
        return response()->json(['response' => $response]);
    }
}
