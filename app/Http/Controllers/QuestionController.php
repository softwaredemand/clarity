<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Validator;
use Auth;
use App\QuestionTopic;
use App\Topic;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::select('id','body')->with('answers','answers.user')->get();
        
        return response()->json([
            'questions' => $questions
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'body'  => 'required|min:6',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $validator->errors()
            ], 422);
        }
        
        $q = new Question();
        $q->body = $request->body;
        $q->detail = $request->detail;
        $q->user_id = $request->anonymously ? "anonymous" : null;
        $q->status = 1;
        $q->save();

        if($request->topics)
        {
            $topics = $request->topics;
            foreach($topics as $t){
                $topic = Topic::where('name', $t)->first();
                QuestionTopic::insert([
                    'question_id' => $q->id,
                    'topic_id' => $topic->id
                ]);
            }
        }

        return response()->json([
            'message' => 'your Question have been posted',
            'question_id' => $q->id
        ], 200);
    }

    public function questions()
    {
        $questions = Question::select('id','body','created_at')->with('answers','answers.user','question_topics','question_topics.topic')->get();
        $question_topics = QuestionTopic::whereIn('question_id',$questions->pluck('id'))->get();
        $question_topics = $question_topics->load('topic')->groupBy('question_id');
        return view('pages.questions', compact('questions','question_topics'));
    }
}
