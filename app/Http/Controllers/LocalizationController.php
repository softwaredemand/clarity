<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;
use Artisan;

class LocalizationController extends Controller
{
    public function setLang($locale)
    {
        Session::put("locale", $locale);
        return redirect()->back();
    }
}
