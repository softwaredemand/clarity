<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Conversation;

class Expertise extends Model
{
    protected $fillable = [
        'user_id', 'title', 'description','category_id','image','status'
    ];

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function conversations()
    {
        return $this->hasMany(Conversation::class);
    }
}
