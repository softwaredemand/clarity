<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Storage;
use App\Expertise;
use App\Conversation;
use App\CallRequest;
use App\Booking;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'google_id', 'facebook_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $timestamps = false;

    public function expertises()
    {
        return $this->hasMany(Expertise::class);
    }

    public function conversations()
    {
        return $this->hasMany(Conversation::class);
    }

    public function call_requests()
    {
        return $this->hasMany(CallRequest::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function short_name(){
        $words = explode(" ", $this->name); 

        $short_name = "";
        foreach ($words as $w) {
            $short_name .= ucwords($w[0]);
        }
        return $short_name;
    }

    public function profileImage(){    
        return $this->profile_image ? asset('storage/images/profile/'.$this->profile_image) : url('images/logo.png');
    }
}
