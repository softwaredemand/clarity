<?php

namespace App;
use App\Question;
use App\Topic;
use Illuminate\Database\Eloquent\Model;

class QuestionTopic extends Model
{
    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }
}
