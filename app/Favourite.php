<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Favourite extends Model
{
	 use SoftDeletes;

    public $timestamps = true;
 	
 	protected $dates = ['deleted_at'];
}
