<?php
use App\Category;
use App\Image;

class Helper{
    
    public static function categories(){
        return Category::select('key','name')->get();
    }

    public static function expertiseImages(){
        $expertise_images = Image::select('imageable_id', 'image')->where('imageable_type', 'expertise')->get();

        foreach($expertise_images as $row)
        {
            $row->image = $row['image'] ? asset('storage/images/expertise/'.$row['image']) : 'images/img00.jpg';
        }
        return $expertise_images;
    }

    public static function makeUserProfileUrl($arr){
        foreach($arr as $row)
        {
            $row->profile_image = $row->profile_image ? asset('storage/images/profile/'.$row->profile_image) : 'images/img1.png';
        }
        return $arr;
    }
}