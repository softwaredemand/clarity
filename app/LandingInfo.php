<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandingInfo extends Model
{
    protected $table = 'landing_info';
}
