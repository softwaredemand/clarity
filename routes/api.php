<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::post('login', 'App\Http\Controllers\Auth\LoginController@login');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('testing',  function ()
{	
	print_r('skldjf'); die();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login-user', 'AuthController@login');
    Route::post('signup-user', 'AuthController@signup');

    Route::get('users', 'AuthController@getalluser'); //getAllUsers
    Route::get('edit-user/{id}', 'AuthController@edituser');
    Route::post('update-user/{id}', 'AuthController@updateuser');
    Route::post('delete-user/{id}', 'AuthController@deleteuser');

    Route::post('update-password/{id}', 'AuthController@updatePassword');
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });

    Route::post('ask-question', 'QuestionController@store');
    Route::get('questions', 'QuestionController@index');
});

Route::group([
    'prefix' => 'topics'
], function () {
    Route::get('get-all-topics', 'TopicController@allTopics');
    Route::post('save-topic', 'TopicController@saveTopic');
    Route::post('save-user-interested-topic', 'TopicController@saveUserInterestedTopic');
    Route::post('user-topics', 'TopicController@userTopics');
});

Route::group([
    'prefix' => 'favorites'
], function () {
    Route::match(['get', 'post'], 'my-favorites', 'FavouriteController@index');
    Route::post('add-to-favorites', 'FavouriteController@store');
    Route::post('remove-to-favorites', 'FavouriteController@destroy');
});

Route::group([
    'prefix' => 'reviews'
], function () {
    Route::match(['get', 'post'], 'get-reviews', 'ReviewController@index');
    Route::post('save-review', 'ReviewController@store');
});

Route::group([
    'prefix' => 'inbox'
], function () {
    Route::post('send', 'ConversationController@store');
    Route::match(['get', 'post'], 'show', 'ConversationController@show');
    Route::match(['get', 'post'], 'my-inbox', 'ConversationController@authUserInbox');
});

Route::post('profile/info','AccountController@update_info');
Route::post('profile/image','AccountController@update_image');
// Route::post('account/apply-for-expert','AccountController@apply_for_expert');
Route::post('expertise/add','ExpertiseController@expertise_update');
Route::post('expertise/hourly_rate','ExpertiseController@hourly_update');
Route::match(['get', 'post'], 'browse/browse-expertise/{category?}','ExpertiseController@allExpertise');

Route::get('get-categories', 'LandingController@getCategories');
Route::post('save-answer', 'AnswerController@store');

Route::post('delete-account', 'SettingController@deleteUser');
Route::post('update-call-request-status','CallRequestController@updateStatus');