<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// use App\Models\User;
// Route::get('boom/{id}', function($id){

// 	$user = User::find($id);    
// 	$user->delete();
// 	echo  $user."deleted"; 
// });

Auth::routes();
Route::group(['middleware'=>'language'],function ()
{
    Route::get('/', 'LandingController@index');
    Route::match(['get', 'post'],'/search', 'LandingController@search')->name('search');
    Route::get('/browse', 'LandingController@browse');
    Route::get('/browse/{cateogory}', 'LandingController@browse_category');
    Route::get('questions', 'QuestionController@questions');
    Route::get('answer-details/{questionId}', 'AnswerController@show');
    Route::get('quote-answer/{questionId?}', 'AnswerController@create');
    Route::get('question/new', 'LandingController@qustionNew');

    Route::get('{source}/booking', 'BookingController@booking');
    Route::post('booking', 'BookingController@store')->name('booking');

    Route::get('customers', 'LandingController@customer');
    Route::get('customers/{name}', 'LandingController@customer_name');
    Route::get('help/article/6/{title}', 'LandingController@help_articles');
    Route::get('help', 'LandingController@help');
    Route::get('how-it-work', 'LandingController@how_it_work');
    Route::get('terms', 'LandingController@terms');

    Route::get('setting', 'SettingController@setting');
    Route::get('setting/notification', 'AccountController@notification');
    Route::get('setting/promotion', 'AccountController@promotion');
    Route::get('setting/password', 'AccountController@changePassword');
    Route::get('setting/earned', 'AccountController@earned');
    Route::get('setting/credit-card', 'AccountController@creditCard');
    Route::get('setting/billing-address', 'AccountController@billingAddress');
    Route::post('billing-info', 'AccountController@storeBillingInfo')->name('billing-info');

    Route::get('/account', 'AccountController@index');
    Route::get('/account/profile', 'AccountController@profile');
    Route::get('/account/profile/info/{anexpert?}', 'AccountController@info');
    Route::get('/account/profile/image', 'AccountController@image');
    Route::get('/account/profile/Verifications', 'AccountController@Verifications');

    Route::get('topics', 'TopicController@topics');
    Route::get('topics/{topicname}', 'TopicController@topicname');
    Route::get('/account/profile/topic', 'TopicController@topic');

    Route::get('/dashboard', 'AccountController@dashboard');

    Route::get('favorites', 'FavouriteController@favorites');
    Route::group([
        'prefix' => 'inbox'
    ], function () {
        Route::get('all', 'ConversationController@index');
        Route::get('start/{expertiseId?}', 'ConversationController@create');
        Route::get('conversation/{conversationId?}', 'ConversationController@create')->name('conversation');
    });
    Route::get('calls', 'BookingController@calls');


    Route::get('/account/profile/expertise', 'ExpertiseController@expertise');
    Route::get('/account/profile/expertise/info/{anexpert?}', 'ExpertiseController@expertise_info');
    Route::get('/account/profile/expertise/video', 'ExpertiseController@expertise_video');
    Route::get('/account/profile/expertise/{createEdit}/{anexpert?}', 'ExpertiseController@expertise_add');
    Route::get('/account/profile/expertise/edit', 'ExpertiseController@expertise_add');
    Route::get('{userid}/expertise/{expertiseid}', 'ExpertiseController@expertiseOpen');

    Route::get('/clear', function () {

        Artisan::call('cache:clear');
        Artisan::call('config:clear');
        Artisan::call('config:cache');
        Artisan::call('view:clear');
        Artisan::call('route:clear');

        return "Cleared!";
    });

    Route::post('stripe', 'AccountController@stripePost')->name('stripe.post');

    Route::get('/user/{username?}', 'AccountController@UserProfileView');

    Route::get('login/{driver}', 'Auth\SocialLoginController@redirectToProvider');
    Route::get('login/{driver}/callback', 'Auth\SocialLoginController@handleProviderCallback');

    Route::get('/home', 'HomeController@index')->name('home');

    //Localization routes
    Route::get('locale/{lang}', 'LocalizationController@setLang');
});