<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
//developer.harismaqsood@gmail.com
    'google' => [
        'client_id' => '88561312323-n5jsf0681uo3kqub7sgl6gc1fdc7u5jk.apps.googleusercontent.com',
        'client_secret' => 'f0Jv6r1btIFW-W5E7xkWsDL9',
        'redirect' => 'https://www.168champion.com/login/google/callback',
    ],
// hariskhokhar.hk@gmail.com
    'facebook'      => [
        'client_id'     => '336479338085172',
        'client_secret' => 'eeb242dbbf05e9376189e51758d1fe82',
        'redirect'      => 'https://www.168champion.com/login/facebook/callback'
    ]

];
