<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/landing', 'HomeController@landing');
Route::post('landing-info', 'HomeController@storeLandingInfo')->name('landing-info');


Route::get('/user-list', 'HomeController@user_list');
Route::get('/user-list/edit/{id}', 'HomeController@userListEdit');
Route::get('/user-list/delete/{id}', 'HomeController@userListDelete');
Route::post('/user-list/update', 'HomeController@userUpdate')->name('user_update');


Route::get('category', 'CategoryController@category_list');
Route::get('category/add', 'CategoryController@categoryAdd');
Route::post('category/addnew', 'CategoryController@categoryAddNew')->name('category_add');
Route::get('category/edit/{id}', 'CategoryController@categoryEdit');
Route::get('category/delete/{id}', 'CategoryController@categoryDelete');
Route::post('category/update', 'CategoryController@categoryUpdate')->name('category_update');


Route::get('topic', 'TopicController@topic_list');
Route::get('topic/add', 'TopicController@topicAdd');
Route::post('topic/addnew', 'TopicController@topicAddNew')->name('topic_add');
Route::get('topic/edit/{id}', 'TopicController@topicEdit');
Route::get('topic/delete/{id}', 'TopicController@topicDelete');
Route::post('topic/update', 'TopicController@topicUpdate')->name('topic_update');