<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\LandingInfo;
use Auth;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');   
    }

    public function user_list()
    {
        $users = User::get();
        return view('pages.user_list',compact('users'));
    }
    public function landing()
    {
        $data = LandingInfo::latest('id')->first();
        if($data)
            $data->image = asset('storage/images/landing/'.$data->image);
        return view('pages.landing', compact('data'));
    }

    public function storeLandingInfo(Request $request){

        $fileName = null;
        if ($request->image)
        {
            $image      = $request->file('image');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            
            $img->stream();

            Storage::disk('local')->put('images/landing/'.$fileName, $img, 'public');
        }
        
        LandingInfo::insert([
            'title' => $request->title,
            'image' => $fileName ? $fileName : null,
        ]);

        return back()->with('success','Uploaded');
    }

    public function userListEdit($user_id)
    {
        $user = User::where('id', '=', $user_id)->first();

        return view('pages.edit_user', compact('user'));
    }

    public function userListDelete($user_id)
    {        
        User::where('id', '=', $user_id)->update(['is_deleted' => true]);

        return redirect()->back();
    }

    public function userUpdate(Request $request)
    {        
        $account = array(
            'name' => $request['name'],
            'username' => $request['username'],
            'email' => $request['email'],
            'short_bio' => $request['short_bio'],
            'mini_resume' => $request['mini_resume'],
            'cell_phone' => $request['cell_phone'],
            'is_deleted' => $request['status'] == 1 ? true : false
        );

        User::where('id', '=', $request->user_id)->update($account);
        
        return redirect()->back();
    }
}
