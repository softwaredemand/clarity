<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Topic;
use Auth;

class TopicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function topicAdd()
    {
        return view('pages.topic_add');
    }

    public function topicAddNew(Request $request)
    {
    	$Topic          = new Topic;
		$Topic->key   	= $request->key;
		$Topic->name 	= $request->name;

		$Topic->save();

        return redirect()->back();
		
    }
    public function topic_list()
    {
        $Topic = Topic::get();

        return view('pages.topic_list',compact('Topic'));
    }

    public function topicDelete($id)
    {        
        Topic::where('id', '=', $id)->delete();

        return redirect()->back();
    }

   	public function topicEdit($id)
    {
        $topic = Topic::where('id', '=', $id)->first();

        return view('pages.edit_topic', compact('topic'));
    }

    public function topicUpdate(Request $request)
    {        
        $topic = array(
            'key' => $request['key'],
            'name' => $request['name']
        );

        Topic::where('id', '=', $request->id)->update($topic);
        
        return redirect()->back();
    }
}