<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Category;
use Auth;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function categoryAdd()
    {
        return view('pages.category_add');
    }

    public function categoryAddNew(Request $request)
    {
    	$category          	= new Category;
		$category->key   	= $request->key;
		$category->name 	= $request->name;

		$category->save();

        return redirect()->back();
		
    }
    public function category_list()
    {
        $category = Category::get();

        return view('pages.category_list',compact('category'));
    }

    public function categoryDelete($id)
    {        
        Category::where('id', '=', $id)->delete();

        return redirect()->back();
    }

   	public function categoryEdit($id)
    {
        $category = Category::where('id', '=', $id)->first();

        return view('pages.edit_category', compact('category'));
    }

    public function categoryUpdate(Request $request)
    {        
        $category = array(
            'key' => $request['key'],
            'name' => $request['name']
        );

        category::where('id', '=', $request->id)->update($category);
        
        return redirect()->back();
    }
}