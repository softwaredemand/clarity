@extends('layouts.app')

@section('content')
<div class="main-wrapper">
  <div class="page-wrapper">
      <div class="content">
			@if ($message = Session::get('success'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>{{ $message }}</strong>
				</div>
			@endif
		
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<form action="{{ route('landing-info') }}" method="POST" enctype="multipart/form-data">
		   		@csrf
		        <div class="form-group mb-0">
		            <label>Landing Image</label>
		            <div>
		                <input class="form-control" name="image" type="file">
						@if (@$data['image'])
							<img src="{{@$data['image']}}">
						@endif
		            </div>
		        </div>
				<div class="form-group mb-0">
                    <label>Blog Name</label>
                    <input class="form-control" type="text" name="title" required value="{{@$data['title']}}">
                </div>
                <div class="m-t-20">
                    <button class="btn btn-primary submit-btn">Save</button>
                </div>
	    	</form>
		</div>
  </div>
</div>
@sectionend