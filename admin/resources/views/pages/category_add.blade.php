@extends('layouts.app')

@section('content')
<div class="main-wrapper">
	   <div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <h4 class="page-title">Edit Category</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <form method="POST" action="{{ route('category_add') }}">
                        @csrf
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>key <span class="text-danger">*</span></label>
                                        <input class="form-control" name="key" type="text"
                                        placeholder="Ex: digital-marketing" >
                                    </div>
                                </div>	
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>name</label>
                                        <input class="form-control" name="name" type="text"
                                        placeholder="Ex: Digital Marketing">
                                    </div>
                                </div>
                            </div>
                           
                            <div class="m-t-20 text-center mb-5">
                                <button class="btn btn-primary submit-btn">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
