@extends('layouts.app')

@section('content')
<div class="main-wrapper">
	   <div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <h4 class="page-title">Edit user</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <form method="POST" action="{{ route('user_update') }}">
                        @csrf
                            <div class="row">
                            	<div>
                            		<input type="hidden" name="user_id" value="{{$user['id']}}">
                            	</div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Name <span class="text-danger">*</span></label>
                                        <input class="form-control" name="name" type="text" value="{{$user['name']}}">
                                    </div>
                                </div>	
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input class="form-control" name="username" type="text" value="{{$user['username']}}">
                                    </div>
                                </div>
                     
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Email <span class="text-danger">*</span></label>
                                        <input class="form-control" name="email" type="email" value="{{$user['email']}}">
                                    </div>
                                </div>
                              <!--   <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input class="form-control" type="password" value="{{$user['password']}}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input class="form-control" type="password" 
                                        value="{{$user['confirm_password']}}">
                                    </div>
                                </div> -->
								
								<div class="col-sm-12">
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label>Short Bio</label>
												<input type="text" class="form-control" name="short_bio" 
												 value="{{$user['short_bio']}}">
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label>Mini Resume</label>
												<input type="text" class="form-control" name="mini_resume" 
												 value="{{$user['mini_resume']}}">
											</div>
										</div>
									</div>
								</div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Phone </label>
                                        <input class="form-control" type="text" name="cell_phone">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                	 <div class="form-group">
		                                <label class="display-block">User Want to Delete?</label>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="status" id="patient_inactive" value="false">
											<label class="form-check-label" for="patient_inactive">
											No
											</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="status" id="patient_active" value="true">
											<label class="form-check-label" for="patient_active">
											Yes
											</label>
										</div>
		                            </div>
                                </div>
                                <!-- <div class="col-sm-6">
									<div class="form-group">
										<label>Avatar</label>
										<div class="profile-upload">
											<div class="upload-img">
												<img alt="" src="assets/img/user.jpg">
											</div>
											<div class="upload-input">
												<input type="file" class="form-control">
											</div>
										</div>
									</div>
                                </div> -->
                            </div>
                           
                            <div class="m-t-20 text-center mb-5">
                                <button class="btn btn-primary submit-btn">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
