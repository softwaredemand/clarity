<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <base href="http://127.0.0.1:8000/"> -->
    <!-- <base href="http://localhost/claritynew/"> -->
    <base href=" https://www.168champion.com/">
   

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

    <!-- Styles -->
    @yield('stylesheet')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/templete.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fonts/iconic/css/material-design-iconic-font.min.css') }}">

    <!-- css page inlcude Account -->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
</head>
<body>
    <div id="app">
        <header>
            <div class="Hdr">
                <div class="container-fluid">
                    <div class="Navbar">
                        <nav class="navbar navbar-light">
                            <div class="site-logo">
                                <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ url('images/logo.png') }}" alt="logo" /></a>                   
                            </div>
                            <div class="web-menu">
                            @auth
                                <div class="adsec">
                                    <div class="dropdown show">
                                    <a class="dropdown-toggle" href="/" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ Auth::user()->profileImage() }}" alt=""><span>Me</span></a>
                                    
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="{{url('account')}}">{{ __('account') }}</a>
                                        <a class="dropdown-item" href="calls">{{ __('calls') }}</a>
                                        <a class="dropdown-item" href="{{url('inbox/all')}}">{{ __('inbox') }}</a>
                                        <a class="dropdown-item" href="{{url('favorites')}}">{{ __('favorites') }}</a>
                                        <a class="dropdown-item" href="account/profile">{{ __('edit_profile') }}</a>
                                        <a class="dropdown-item" href="{{url('setting')}}">{{ __('settings') }}</a>
                                        <a class="dropdown-item" href="{{url('help')}}">{{ __('support') }}</a>
                                        <a class="dropdown-item" href="#"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();"
                                                        >{{ __('logout') }}</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                    </div>
                                </div>
                            @endauth
                                <div class="adsec">
                                    <div class="dropdown show">
                                        <a class="dropdown-toggle" href="#" role="button" id="lang" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>Language</span></a>
                                        
                                        <div class="dropdown-menu" aria-labelledby="lang">
                                            <a class="dropdown-item" href="locale/en">English</a>
                                            <a class="dropdown-item" href="locale/es">Spanish</a>
                                        </div>
                                    </div>
                                </div>
                                @guest
                                <ul class="float-right">
                                    <li><a href="{{ route('register') }}">{{ __('sign_up') }}</a></li>
                                    <li><a  href="{{ route('login') }}">{{ __('login') }}</a></li>
                                </ul>
                                @endguest
                                <nav class="navbar-expand-lg navbar-expand-md navbar-dark">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                </button>
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul>
                                    
                                    <li class="dropdown"><a href="#" class="dropbtn">{{ __('categories') }}</a>
                                        <div class="dropdown-content">
                                            @foreach(Helper::categories() as $cat)
                                            <a href="browse/{{$cat['key']}}">{{$cat['name']}}</a>
                                            @endforeach
                                        </div>
                                    </li>
                                    <li class="search-bar">
                                    <form action="{{ route('search') }}" method="POST">
                                        @csrf
                                        <input type="text" placeholder="" name="query">
                                        <input class="fa" type="submit" value="&#xf002">
                                    </form>
                                    </li>
                                    <li><a href="dashboard">{{ __('dashboard') }} </a></li>
                                    <li><a href="browse">{{ __('browse') }}</a></li>
                                    <li><a href="questions">{{ __('answers') }}</a></li>
                                </ul>
                                </div>
                                </nav>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </header>

        <main class="">
            @yield('content')
        </main>
    </div>
    @auth
        <script>
            window.authUser = {!! json_encode([
                'id' => Auth::user()->id
            ]) !!};
        </script>
    @endauth
    <script>
            window.locale = {{ app()->getLocale() }}
        </script>
    @yield('js')
</body>
</html>
