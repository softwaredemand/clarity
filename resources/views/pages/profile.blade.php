@extends('layouts.app')

@section('content')
{{
        Session::has('profileToast')
}}
<profile 
:user="{{ json_encode($User) }}"
profile-toast="{{ Session::has('profileToast') }}" ></profile>
@endsection