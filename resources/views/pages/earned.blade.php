@extends('layouts.app')

@section('content')
  <section>
    	<div class="bnr bnr-sub border-bottom-0">
        	<div class="container">
            	<a href="setting">Back</a>
            </div>
        </div>
    	<div class="bnr2 m-0 pt-4">
        	<div class="container">
            	<div class="toptab">
                    <ul class="nav" role="tablist">
                        <li>
                          <a class="active" data-toggle="tab" href="#earned ">Earned</a>
                        </li>
                        <li>
                          <a data-toggle="tab" href="#paid">Paid</a>
                        </li>
                     </ul>
                 </div>
            </div>
        </div>
        <div class="content-sec">
        	<div class="container">
                <div class="expertse8s">
                	<!-- Tab panes -->
                      <div class="tab-content">
                        <div id="earned" class="container tab-pane active"><br>
                          <div class="row">
                          	<div class="col-lg-4 col-md-4 col-sm-12 float-left">
                            	<h3>Account Balance</h3>
                                <div class="balancesec">
                                	<form>
                                    	<span><input class="optional" type="text" name="payout" placeholder="$0.00" pattern="^\$?(\d|,)*\.?\d{0,2}$" value="{{$earnings}}" disabled=""></span>
                                        <input type="submit" value="Request Withdrawal">
                                    </form>
                                </div>
                                <h4>Total earned on Clarity: <strong>${{$earnings}}</strong></h4>
                                <p>Since you signed up on May 09, 2021</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 float-left">
                            	<h3>Transactions</h3>
                                <div class="no-transactions">
                                  There are no transactions yet.
                                </div>
                            </div>
                          </div>
                        </div>
                        <div id="paid" class="container tab-pane fade"><br>
                          <div class="row">
                          	<div class="col-lg-4 col-md-4 col-sm-12 float-left">
                            	<h3>Account Balance</h3>
                                <div class="balancesec">
                                	<form>
                                    	<span><input class="optional" type="text" name="payout" placeholder="$0.00" pattern="^\$?(\d|,)*\.?\d{0,2}$" value="{{$earnings}}" disabled=""></span>
                                        <h6>As of May 2021</h6>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 float-left">
                            	<h3>Transactions</h3>
                                <div class="no-transactions">
                                  There are no transactions yet.
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </section>
@endsection
