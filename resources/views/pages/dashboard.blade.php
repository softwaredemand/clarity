@extends('layouts.app')

@section('content')
<section>
    <div class="bnr2">
        <div class="container">
            <div class="toptab2">
                <ul>
                    <li>
                        <a href="/account/profile">
                            @if(Auth::user()->profile_image)
                                <span class="image">
                                    <img src="{{Auth::user()->profileImage()}}" alt="" class="img-round">
                                </span>
                            @else
                                <span>{{Auth::user()->short_name()}}</span>
                            @endif
                            <b>Hello,</b>
                            <strong>{{Auth::user()->name}}</strong>
                        </a>
                    </li>
                    <li>
                        <a href="/calls">
                            <div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                            <strong>Call Requests</strong>
                            <h4>No new call request</h4>
                        </a>
                    </li>
                    <li>
                        <a href="/inbox/all">
                            <div class="icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                            <strong>Inbox</strong>
                            <h4>No new messages</h4>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="content-sec">
        <div class="container">
            <div class="expertsec3s">
                <recent-activities :recent_activities="{{$recent_activities}}"></recent-activities>
                <popular-answers></popular-answers>
            </div>
        </div>
    </div>
</section>
@endsection