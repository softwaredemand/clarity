@extends('layouts.app')

@section('content')
  <section>
    	<div class="bnr2">
        	<div class="container">
            	<div class="toptab3s">
                	<h1>Terms of Service</h1>
                 </div>
            </div>
        </div>
        <div class="content-sec">
        	<div class="container">
                <div class="experts3s">
                	<h3>Acceptance of Terms</h3>
      				<p>Welcome to Clarity, owned and operated by Fundable LLC (collectively, <strong>“Clarity”</strong>).  Clarity provides an online venue and services that connect users providing information and advice (the <strong>“Experts”</strong>) with users seeking information and advice (the <strong>“Seekers”</strong>) (collectively, the <strong>“Services”</strong>), which Services are accessible at <a href="http://www.clarity.fm" class="allow-default">www.clarity.fm</a> and any other websites through which Clarity makes the Services available (collectively, the <strong>“Site”</strong>) and as an application for mobile devices (the <strong>“Application”</strong>). By using the Site and Application, you agree to comply with and be legally bound by the terms and conditions of these Terms of Service (<strong>“Terms”</strong>), whether or not you become a registered user of the Services. These Terms govern your access to and use of the Site, Application and Services and all Collective Content (as defined below) and constitute a binding legal agreement between you and Clarity.
        </p>
        <p>
        In these Terms, <strong>“you”</strong> and <strong>“your”</strong> refer to the individual or entity that uses the Site, Application or Services. <strong>“We”</strong>, <strong>“us”</strong>, or <strong>“our”</strong> refer to Clarity. In addition, in these Terms, unless the context requires otherwise, words in one gender include all genders and words in the singular include the plural and vice-versa.
        </p>
        <p>
        Please read carefully these Terms and our Privacy Policy, which may be found at <a href="http://www.clarity.fm/terms" class="allow-default">www.clarity.fm/terms</a>, and which is incorporated by reference into these Terms. If you do not agree to these Terms, you have no right to obtain information from or otherwise continue using the Site or Application. Failure to use the Site and Application in accordance with these Terms may subject you to civil and criminal penalties.
        </p>
        <p>
        THE SITE, APPLICATION AND SERVICES COMPRISE AN ONLINE PLATFORM THROUGH WHICH EXPERTS MAY CREATE LISTINGS (AS DEFINED BELOW) IN ORDER TO OFFER INFORMATION AND ADVICE TO SEEKERS. YOU UNDERSTAND AND AGREE THAT CLARITY IS NOT A PARTY TO ANY AGREEMENTS ENTERED INTO BETWEEN MEMBERS (AS DEFINED BELOW). CLARITY HAS NO CONTROL OVER THE CONDUCT OF MEMBERS OR OTHER USERS OF THE SITE, APPLICATION AND SERVICES OR ANY INFORMATION PROVIDED IN CONNECTION THERETO, AND DISCLAIMS ALL LIABILITY IN THIS REGARD.
        </p>

        <h3>Definitions</h3>

        <p>
          <strong>“Appointment”</strong> means a scheduled telephone call coordinated via the Services between Members in connection with and subject to the terms of a Listing.
          </p>
          <p>
          <strong>“Clarity Content”</strong> means all Content that Clarity makes available through the Site, Application, or Services, including any Content licensed from a third party, but excluding Member Content.
          </p>
          <p>
          <strong>“Collective Content”</strong> means Member Content and Clarity Content.
          </p>
          <p>
          <strong>“Content”</strong> means text, graphics, images, music, software (excluding the Application), audio, video, information or other materials.
          </p>
          <p>
          <strong>“Expert”</strong> means a Member who offers information and advice to other Members and creates a Listing via the Site, Application or Services.
          </p>
          <p>
          <strong>“Listing”</strong> means an offer by an Expert to provide the Services via the Site, Application, and Services.
          </p>
          <p>
          <strong>“Member”</strong> means a person who completes Clarity’s account registration process as described under “User Account Registration” below, and includes, without limitation, Experts and Seekers.
          </p>
          <p>
          <strong>“Member Content”</strong> means all Content that a Member posts, uploads, publishes, submits or transmits to be made available through the Site, Application or Services and includes, without limitation, Listings.
          <strong>“Seeker”</strong> means a Member who uses the Site, Application or Services to access information and advice from other Members.
          </p>
          <p>
          <strong>“Tax”</strong> or <strong>“Taxes”</strong> mean any sales taxes, value added taxes (VAT), goods and services taxes (GST) and other similar municipal, provincial, state and federal indirect or other withholding and personal or corporate income taxes.
          </p>
          <p>
          Certain areas of the Site and Application (and your access to or use of certain aspects of the Services or Collective Content) may have different terms and conditions posted or may require you to agree with and accept additional terms and conditions. If there is a conflict between these Terms and terms and conditions posted for a specific area of the Site, Application, Services, or Collective Content, the latter terms and conditions will take precedence with respect to your use of or access to that area of the Site, Application, Services, or Collective Content.
          </p>
          <p>
          YOU ACKNOWLEDGE AND AGREE THAT, BY ACCESSING OR USING THE SITE, APPLICATION OR SERVICES OR BY DOWNLOADING OR POSTING ANY CONTENT FROM OR ON THE SITE, VIA THE APPLICATION OR THROUGH THE SERVICES, YOU ARE INDICATING THAT YOU HAVE READ, AND THAT YOU UNDERSTAND AND AGREE TO BE BOUND BY THESE TERMS, WHETHER OR NOT YOU HAVE REGISTERED WITH THE SITE AND APPLICATION. IF YOU DO NOT AGREE TO THESE TERMS, THEN YOU HAVE NO RIGHT TO ACCESS OR USE THE SITE, APPLICATION, SERVICES OR COLLECTIVE CONTENT. If you accept or agree to these Terms on behalf of a company or other legal entity, you represent and warrant that you have the authority to bind that company or other legal entity to these Terms and, in such event, “you” and “your” will refer and apply to that company or other legal entity.
          </p>

          <h3>Modification</h3>

          <p>
            Clarity reserves the right, at its sole discretion, to modify the Site, Application or Services or to modify these Terms, including the Service Fee (as defined below), at any time and without prior notice. If we modify these Terms, we will post the modification on the Site or via the Application. We will also update the “Last Updated Date” at the top of these Terms.  Modifications to these Terms shall automatically be effective upon posting; provided, however, that material changes to the Terms will be effective as to an existing Member thirty (30) days after notice to the Member is provided via email from <a href="mailto:support@clarity.fm" class="allow-default">support@clarity.fm</a> to your email address on file with Clarity. You agree to keep your email address on file with Clarity up-to-date. By continuing to access or use the Site, Application or Services after we have posted a modification on the Site or via the Application or have provided you with notice of a modification, you are indicating that you agree to be bound by the modified Terms. If the modified Terms are not acceptable to you, your only recourse is to cease using the Site, Application and Services.
          </p>

          <h3>Eligibility</h3>

          <p>
            The Site, Application and Services are intended solely for persons who are 18 or older. Any access to or use of the Site, Application or Services by anyone under 18 is expressly prohibited. By accessing or using the Site, Application or Services you represent and warrant that (i) you agree to be bound by these Terms and (ii) that you are 18 or older and able to form legally binding contracts.
          </p>

          <h3>How the Site, Application and Services Work</h3>
          <p>
          The Site is a platform for Members to connect and schedule Appointments in order to exchange information with other Members.  You may view Listings as an unregistered visitor to the Site, Application and Services; however, if you wish to use the Services or post a Listing, you must first register to create a Clarity Account (as defined below).
          </p>
          <p>
            Clarity’s role is solely to facilitate the availability of the Site, Application and Services and to provide services related thereto, such as Appointment scheduling, call queuing, call cycling, payment integration and call facilitation (see <a href="http://www.clarity.fm/features" class="allow-default">www.clarity.fm/features</a> for more details). Clarity does not provide and is not responsible for Member Content or any information or advice exchanged between Members during Appointments or otherwise.  Clarity does not verify the credentials of any of its Members. You understand and acknowledge that Experts are not employees or agents of Clarity but are independent service providers using the Site, Application and Services to market their expertise to other Members and the public. You acknowledge that Clarity will not be liable for any loss or damage caused by your reliance on information provided by Members or information contained in Member Content.
          </p>
          <p>PLEASE NOTE THAT, AS STATED ABOVE, THE SITE, APPLICATION AND SERVICES ARE INTENDED TO BE USED TO FACILITATE MEMBER INTERACTION. CLARITY CANNOT AND DOES NOT CONTROL OR GUARANTEE THE CONTENT CONTAINED IN ANY LISTINGS OR THE INFORMATION EXCHANGED BETWEEN MEMBERS VIA THE SERVICES.  YOU UNDERSTAND AND ACKNOWLEDGE THAT CLARITY IS NOT RESPONSIBLE FOR AND DISCLAIMS ANY AND ALL LIABILITY RELATED TO ANY AND ALL LISTINGS AND INFORMATION PROVIDED UNDER THE SERVICES. ACCORDINGLY, ALL MEMBERS USE THE SITE, APPLICATION AND SERVICES AT THEIR OWN RISK.
          </p>

          <h3>User Account Registration</h3>

          <p>
          <em>Direct Registration:</em> To access certain features of the Site and Application and to create a Listing, you must register to create an account (<strong>“Clarity Account”</strong>) and become a Member. You may register directly via the Site or Application or as described in this section.
          </p>
          <p>
          <em>Third Party Registration:</em> You can also register to join by logging into your account with certain third party social networking sites (<strong>“SNS”</strong>) (including, but not limited to, Facebook, Twitter and LinkedIn); each such account, a <strong>“Third Party Account”</strong>, via our Site or Application, as described below.
          </p>
          <p>
          As part of the functionality of the Site, Application and Services, you may link your Clarity Account with Third Party Accounts, by either: (i) providing your Third Party Account login information to Clarity through the Site, Services or Application; or (ii) allowing Clarity to access your Third Party Account, as is permitted under the applicable terms and conditions that govern your use of each Third Party Account. You represent that you are entitled to disclose your Third Party Account login information to Clarity and/or grant Clarity access to your Third Party Account (including, but not limited to, for use for the purposes described herein), without breach by you of any of the terms and conditions that govern your use of the applicable Third Party Account and without obligating Clarity to pay any fees or making Clarity subject to any usage limitations imposed by such third party service providers.
          </p>
          <p>
          By granting Clarity access to any Third Party Accounts, you understand that Clarity will access, make available and store (if applicable) any Content that you have provided to and stored in your Third Party Account (<strong>“SNS Content”</strong>) so that it is available on and through the Site, Services and Application via your Clarity Account and Clarity Account profile page. Unless otherwise specified in these Terms, all SNS Content, if any, will be considered to be Member Content for all purposes of these Terms. Depending on the Third Party Accounts you choose and subject to the privacy settings that you have set in such Third Party Accounts, personal information that you post to your Third Party Accounts will be available on and through your Clarity Account on the Site, Services and Application. Please note that if a Third Party Account or associated service becomes unavailable, or Clarity’s access to such Third Party Account is terminated by the third party service provider, then SNS Content will no longer be available on and through the Site, Services and Application. You have the ability to disable the connection between your Clarity Account and your Third Party Accounts, at any time, by accessing the “Settings” section of the Site and Application.
          </p>
          <p>
          PLEASE NOTE THAT YOUR RELATIONSHIP WITH THE THIRD PARTY SERVICE PROVIDERS ASSOCIATED WITH YOUR THIRD PARTY ACCOUNTS IS GOVERNED SOLELY BY YOUR AGREEMENT(S) WITH SUCH THIRD PARTY SERVICE PROVIDERS. Clarity makes no effort to review any SNS Content for any purpose, including but not limited to, for accuracy, legality or non-infringement and Clarity is not responsible for any SNS Content.
          </p>
          <p>
          We will create your Clarity Account and your Clarity Account profile page for your use of the Site and Application based upon the personal information you provide to us or that we obtain via an SNS as described above. You may not have more than one (1) active Clarity Account. You agree to provide accurate, current and complete information during the registration process and to update such information to keep it accurate, current and complete. Clarity reserves the right to suspend or terminate your Clarity Account and your access to the Site, Application and Services if you create more than one (1) Clarity Account or if any information provided during the registration process or thereafter proves to be inaccurate, not current or incomplete. You are responsible for safeguarding your password. You agree that you will not disclose your password to any third party and that you will take sole responsibility for any activities or actions under your Clarity Account, whether or not you have authorized such activities or actions. You will immediately notify Clarity of any unauthorized use of your Clarity Account.
          </p>

          <h3>Listings</h3>

          <p>
            You (as an Expert) may create Listings to offer your services to other Members. To this end, you may need to provide information regarding the expertise you offer as well as pricing and other financial terms applicable to your offering. Listings will be made publicly available via the Site, Application and Services.
            </p>
            <p>
            Seekers will be able to schedule an Appointment with you via the Site, Application and Services based upon the information provided in your Listing.  You understand and agree that once a Seeker requests an Appointment, the price quoted under your Listing may not be altered.
            </p>
            <p>
            You acknowledge and agree that you are solely responsible for any and all Listings you post. Accordingly, you represent and warrant that any Listing you post and the agreements you enter into with Seekers (i) will not breach any agreements you have entered into with any third parties and (ii) will (a) comply with all applicable laws, Tax requirements, and rules and regulations that may apply to you and (b) not conflict with the rights of third parties. Please note that Clarity assumes no responsibility for the content of Listings or for any Member’s compliance with any applicable laws, rules and regulations.
            </p>
            <p>
            You understand and agree that Clarity is not involved in the interactions between Members and does not refer or endorse or recommend particular Experts to Seekers.  You also understand and acknowledge that Clarity does not edit, modify, filter, screen, monitor, endorse or guarantee Member Content or the content of communications between Members.  As stated above, Clarity is not party to any agreements entered into between Members.
            </p>
            <p>
            Clarity reserves the right, at any time and without prior notice, to remove or disable access to any Listing for any reason, including Listings that Clarity, in its sole discretion, considers to be objectionable for any reason, in violation of these Terms or otherwise harmful to the Site, Application or Services.  You acknowledge and agree that, as an Expert, you are responsible for your own acts and omissions.
            </p>

          <h3>No Endorsement</h3>

          <p>
            As stated above, Clarity does not endorse any of its Members. In addition, although these Terms require Members to provide accurate information, we do not attempt to confirm, and do not confirm, any Member’s purported identity or credentials. You are responsible for determining the identity and suitability of others who you contact via the Site, Application and Services.
            </p>
            <p>
            By using the Site, Application or Services, you agree that any legal remedy or liability that you seek to obtain for actions or omissions of other Members or other third parties will be limited to a claim against the particular Members or other third parties who caused you harm and you agree not to attempt to impose liability on, or seek any legal remedy from Clarity with respect to such actions or omissions.
            </p>

          <h3>Appointments and Financial Terms</h3>

          <p>
            Appointments and Financial Terms for Experts:
            If you (as an Expert) have received an Appointment request from a Member, you will be required to either confirm or reject the request within 24 hours of when the request is made (as determined by Clarity in its sole discretion) or the request will be automatically cancelled. When an Appointment is requested with you via the Site, Application and Services, we will share with you (i) the first and last name of the Seeker who has requested the Appointment, and (ii) a link to the Seeker’s Clarity Account profile page, so that you can view this information before confirming or rejecting the Appointment. When you confirm an Appointment request, Clarity will send you an email, text message or message via the Application confirming such booking, depending on the selections you make via the Site, Application and Services.
            </p>
            <p>
            The amount due and payable by a Seeker relating to Appointment time with you is referred to as an <strong>“Appointment Fee”</strong>. Appointment Fees are quoted in each Listing, in U.S. dollars and in a price per minute format (the <strong>“Appointment Fee Rate”</strong>).  Please note that it is Experts, and not Clarity, which set all Appointment Fee Rates.
            </p>
            <p>
            In consideration of the Services, Clarity charges you a fee (the <strong>“Service Fee”</strong>) based on a percentage of Appointment Fees collected on your behalf. The current applicable percentage is fifteen percent (15%). The Service Fee is deducted from the Appointment Fee payable to you in respect of an Appointment. At the conclusion of each Appointment, Clarity calculates the appropriate Appointment Fee payable by the Seeker to you based on the duration of the Appointment and the applicable Appointment Fee Rate.  After deducting the applicable Service Fee, Clarity remits the balance of the Appointment Fee to you via its third party provider <a href="http://www.stripe.com" class="allow-default">www.stripe.com</a> or such other payment methods as may be listed on the Site or via the Application, in U.S. dollars. Except as otherwise provided herein, Service Fees are non-refundable.
            </p>
            <p>
            Please note that Clarity does not currently charge fees for the creation of Listings. However, you acknowledge and agree that Clarity reserves the right, in its sole discretion, to charge you for and collect fees from you for the creation of Listings. Please note that Clarity will provide notice of any Listing fee collection via the Site, Application and Services, in accordance with these Terms, prior to implementing such a Listing fee feature.
            </p>

            <p>

              Appointments and Financial Terms for Seekers
              </p>
              <p>
              You (as a Seeker), not Clarity, are solely responsible for honoring any confirmed Appointments.  If you choose to enter into a transaction with an Expert by scheduling an Appointment via the Site or Application, these Terms and other terms, conditions, rules and restrictions associated with such Appointment as set out in the Listing may apply. You acknowledge and agree that you, and not Clarity, will be responsible for performing the obligations of any such agreements, and Clarity is not a party to such agreements and disclaims all liability arising from or related to any such agreements.
              </p>
              <p>
              You agree to pay Clarity all Appointment Fees due in connection with any Appointment. In order to initiate an Appointment, you understand and agree that Clarity reserves the right, in its sole discretion, to obtain a pre-authorization of your credit card or charge your credit card a nominal amount, not to exceed one U.S. dollar ($1), in order to verify your credit card. At the end of each Appointment, Clarity will process and collect the Appointment Fees payable in accordance with these Terms and the terms of the Listing. Please note that Clarity cannot control any fees that may be charged to a Member by his or her bank related to Clarity’s collection of the Appointment Fees, and Clarity disclaims all liability in this regard.
              </p>
              <p>
              In connection with your payment, you will be asked to provide customary billing information such as name, billing address and credit card information either to Clarity or its third party payment processor. You agree to pay Clarity for any consummated Appointments in accordance with these Terms, by one of the methods described on the Site or Application – e.g. by <a href="http://www.stripe.com" class="allow-default">www.stripe.com</a>. You hereby authorize the collection of such amounts by charging the credit card provided as part of requesting the Appointment, either directly by Clarity or indirectly, via a third party online payment processor or by one of the payment methods described on the Site or Application.  If you are directed to Clarity’s third party payment processor, you may be subject to terms and conditions governing use of that third party’s service and that third party’s personal information collection practices. Please review such terms and conditions and privacy policy before using the services. Once your transaction is complete you will receive a confirmation email summarizing your confirmed Appointment.
              </p>

            <h3>General Financial Terms</h3>

            <p>
              Refunds
               </p>
                <p>
              You (as a Seeker) may cancel an Appointment without penalty; provided, however, that you have not already initiated the call with the Expert via the Services. You (as an Expert) may cancel a scheduled Appointment without penalty at any time.  If you have been improperly charged for an Appointment that was cancelled and require a refund, please contact Clarity at <a href="mailto:support@clarity.fm" class="allow-default">support@clarity.fm</a>.
               </p>
                <p>
              Taxes
               </p>
                <p>
              You understand and agree that you are solely responsible for determining your applicable Tax reporting requirements in consultation with your tax advisors.  Clarity cannot and does not offer Tax-related advice to any Members of the Site, Application and Services.
            </p>


            <h3>User Conduct</h3>

            You understand and agree that you are solely responsible for compliance with any and all laws, rules, regulations, and Tax obligations that may apply to your use of the Site, Application, Services and Content. In connection with your use of our Site, Application and Services, you may not and you agree that you will not:
            <p></p>
            <p>
            </p><ul>
            <li>
            violate any local, state, provincial, national, or other law or regulation, or any order of a court, including, without limitation, zoning restrictions and Tax regulations;
            </li>
            <li>use manual or automated software, devices, scripts robots, other means or processes to access, “scrape”, “crawl” or “spider” any web pages or other services contained in the Site, Application, Services or Content;
            </li>
            <li>use the Site, Application or Services for any commercial or other purposes that are not expressly permitted by these Terms;
            copy, store or otherwise access any information contained on the Site, Application, Services or Content for purposes not expressly permitted by these Terms;
            </li>
            <li>infringe the rights of any person or entity, including without limitation, their intellectual property, privacy, publicity or contractual rights;
            </li>
            <li>interfere with or damage our Site, Application or Services, including, without limitation, through the use of viruses, cancel bots, Trojan horses, harmful code, flood pings, denial-of-service attacks, packet or IP spoofing, forged routing or electronic mail address information or similar methods or technology;
            </li>
            <li>use our Site, Application or Services to transmit, distribute, post or submit any information concerning any other person or entity, including without limitation, photographs of others without their permission, personal contact information or credit, debit, calling card or account numbers;
            </li>
            <li>use our Site, Application or Services in connection with the distribution of unsolicited commercial email ("spam") or advertisements unrelated to lodging in a private residence;
            </li>
            <li>"stalk" or harass any other user of our Site, Application, or Services or collect or store any personally identifiable information about any other user other than for purposes of transacting as a Clarity Member;
            </li>
            <li>register for more than one Clarity Account or register for a Clarity Account on behalf of an individual other than yourself;
            </li>
            <li>contact an Expert for any purpose other than asking a question related to the Services,
            </li>
            <li>contact a Seeker for any purpose other than asking a question related to the Services;
            </li>
            <li>recruit or otherwise solicit any other Member to join third party services or websites that are competitive to Clarity, without Clarity’s prior written approval;
            </li>
            <li>impersonate any person or entity, or falsify or otherwise misrepresent yourself or your affiliation with any person or entity;
            </li>
            <li>use automated scripts to collect information or otherwise interact with the Site, Application or Services;
            </li>
            <li>use the Site, Application and Services to find an Expert and then complete a transaction independent of the Site, Application or Services in order to circumvent the obligation to pay any fees related to Clarity’s provision of the Services;
            </li>
            <li>as an Expert, submit any Listing with a false or misleading information, or submit any Listing with a price that you do not intend to honor;
            </li>
            <li>post, upload, publish, submit or transmit any Content that: (i)infringes, misappropriates or violates a third party’s patent, copyright, trademark, trade secret, moral rights or other intellectual property rights, or rights of publicity or privacy; (ii) violates, or encourages any conduct that would violate, any applicable law or regulation or would give rise to civil liability; (iii) is fraudulent, false, misleading or deceptive; (iv) is defamatory, obscene, pornographic, vulgar or offensive; (v)promotes discrimination, bigotry, racism, hatred, harassment or harm against any individual or group; (vi) is violent or threatening or promotes violence or actions that are threatening to any other person; or (vii) promotes illegal or harmful activities or substances;
            </li>
            <li>systematically retrieve data or other content from our Site, Application or Services to create or compile, directly or indirectly, in single or multiple downloads, a collection, compilation, database, directory or the like, whether by manual methods, through the use of bots, crawlers, or spiders, or otherwise;
            </li>
            <li>use, display, mirror or frame the Site or Application, or any individual element within the Site, Services, or Application, Clarity’s name, any Clarity trademark, logo or other proprietary information, or the layout and design of any page or form contained on a page, without Clarity’s express written consent;
            </li>
            <li>access, tamper with, or use non-public areas of the Site or Application, Clarity’s computer systems, or the technical delivery systems of Clarity’s providers;
            </li>
            <li>attempt to probe, scan, or test the vulnerability of any Clarity system or network or breach any security or authentication measures;
            </li>
            <li>avoid, bypass, remove, deactivate, impair, descramble, or otherwise circumvent any technological measure implemented by Clarity or any of Clarity’s providers or any other third party (including another user) to protect the Site, Services, Application or Collective Content;
            </li>
            <li>forge any TCP/IP packet header or any part of the header information in any email or newsgroup posting, or in any way use the Site, Services, Application or Collective Content to send altered, deceptive or false source-identifying information;
            </li>
            <li>attempt to decipher, decompile, disassemble or reverse engineer any of the software used to provide the Site, Services, Application or Collective Content; or
            </li>
            <li>advocate, encourage, or assist any third party in doing any of the foregoing.
            </li>
            </ul>
            <p></p>
            <p>Clarity will have the right to investigate and prosecute violations of any of the above to the fullest extent of the law. Clarity may involve and cooperate with law enforcement authorities in prosecuting users who violate these Terms. You acknowledge that Clarity has no obligation to monitor your access to or use of the Site, Application, Services or Collective Content or to review or edit any Member Content, but has the right to do so for the purpose of operating the Site, Application and Services, to ensure your compliance with these Terms, or to comply with applicable law or the order or requirement of a court, administrative agency or other governmental body. Clarity reserves the right, at any time and without prior notice, to remove or disable access to any Collective Content that Clarity, at its sole discretion, considers to be objectionable for any reason, in violation of these Terms or otherwise harmful to the Site, Application or Services.
            </p>

          <h3>Privacy</h3>

          <p>
            See Clarity’s Privacy Policy at <a href="http://www.clarity.fm/terms" class="allow-default">www.clarity.fm/terms</a> for information and notices concerning Clarity’s collection and use of your personal information.</p>

          <h3>Ownership</h3>

          <p>
            The Site, Application, Services, and Collective Content are protected by copyright, trademark, and other laws of the United States and foreign countries. You acknowledge and agree that the Site, Application, Services and Collective Content, including all associated intellectual property rights is the exclusive property of Clarity and its licensors. You will not remove, alter or obscure any copyright, trademark, service mark or other proprietary rights notices incorporated in or accompanying the Site, Application, Services, or Collective Content.
          </p>

          <h3>Application License</h3>

          <p>
            Subject to your compliance with these Terms, Clarity grants you a limited non-exclusive, non-transferable license to download and install a copy of the Application on a single mobile device or computer that you own or control and run such copy of the Application solely for your own personal use. Clarity reserves all rights in the Application not expressly granted to you by these Terms.
          </p>

          <h3>Clarity Content and Member Content License</h3>

          <p>
            Subject to your compliance with the terms and conditions of these Terms, Clarity grants you a limited, non-exclusive, non-transferable license, to (i) access and view any Clarity Content solely for your personal and non-commercial purposes and (ii) access and view any Member Content to which you are permitted access, solely for your personal and non-commercial purposes. You have no right to sublicense the license rights granted in this section.</p>
            <p>You will not use, copy, adapt, modify, prepare derivative works based upon, distribute, license, sell, transfer, publicly display, publicly perform, transmit, broadcast or otherwise exploit the Site, Application, Services, or Collective Content, except as expressly permitted in these Terms. No licenses or rights are granted to you by implication or otherwise under any intellectual property rights owned or controlled by Clarity or its licensors, except for the licenses and rights expressly granted in these Terms.
          </p>

          <h3>Member Content</h3>

          <p>
          We may, in our sole discretion, permit Members to post, upload, publish, submit or transmit Member Content. By making available any Member Content on or through the Site, Application and Services, you hereby grant to Clarity a worldwide, irrevocable, perpetual, non-exclusive, transferable, royalty-free license, with the right to sublicense, to use, view, copy, adapt, modify, distribute, license, sell, transfer, publicly display, publicly perform, transmit, stream, broadcast, access, view, and otherwise exploit such Member Content on, through, or by means of the Site, Application and Services. Clarity does not claim any ownership rights in any such Member Content and nothing in these Terms will be deemed to restrict any rights that you may have to use and exploit any such Member Content.
          </p>
          <p>You acknowledge and agree that you are solely responsible for all Member Content that you make available through the Site, Application and Services. Accordingly, you represent and warrant that: (i) you either are the sole and exclusive owner of all Member Content that you make available through the Site, Application and Services or you have all rights, licenses, consents and releases that are necessary to grant to Clarity the rights in such Member Content, as contemplated under these Terms; and (ii) neither the Member Content nor your posting, uploading, publication, submission or transmittal of the Member Content or Clarity’s use of the Member Content (or any portion thereof) on, through or by means of the Site, Application and the Services will infringe, misappropriate or violate a third party’s patent, copyright, trademark, trade secret, moral rights or other proprietary or intellectual property rights, or rights of publicity or privacy, or result in the violation of any applicable law or regulation.
          </p>

          <h3>Links</h3>

          <p>
            The Site, Application and Services may contain links to third-party websites or resources. You acknowledge and agree that Clarity is not responsible or liable for: (i) the availability or accuracy of such websites or resources; or (ii) the content, products, or services on or available from such websites or resources. Links to such websites or resources do not imply any endorsement by Clarity of such websites or resources or the content, products, or services available from such websites or resources. You acknowledge sole responsibility for and assume all risk arising from your use of any such websites or resources or the Content, products or services on or available from such websites or resources.
          </p>

          <h3>Proprietary Rights Notices</h3>

          <p>All trademarks, service marks, logos, trade names and any other proprietary designations of Clarity used herein are trademarks or registered trademarks of Clarity. Any other trademarks, service marks, logos, trade names and any other proprietary designations are the trademarks or registered trademarks of their respective parties.
          </p>

          <h3>Copyright Policy</h3>

          <p>
          Clarity respects copyright law and expects its users to do the same. It is Clarity’s policy to terminate in appropriate circumstances the Clarity Accounts of Members who infringe or are believed to be infringing the rights of copyright holders.
          </p>

          <h3>Termination and Clarity Account Deactivation</h3>

          <p>
            We may, in our discretion and without liability to you, with or without cause, with or without prior notice and at any time: (a) terminate these Terms or your access to our Site, Application and Services, and (b) deactivate or cancel your Clarity Account. Upon termination we will promptly pay you any amounts we reasonably determine we owe you in our discretion, which we are legally obligated to pay you. In the event Clarity terminates these Terms, or your access to our Site, Application and Services or deactivates or cancels your Clarity Account you will remain liable for all amounts due hereunder. You may cancel your Clarity Account at any time by contacting Clarity. Please note that if your Clarity Account is cancelled, we do not have an obligation to delete or return to you any Content you have posted to the Site, Application and Services, including, but not limited to, any reviews or Feedback.
          </p>

          <h3>Disclaimers</h3>

          <p>IF YOU CHOOSE TO USE THE SITE, APPLICATION AND SERVICES, YOU DO SO AT YOUR SOLE RISK. YOU ACKNOWLEDGE AND AGREE THAT CLARITY DOES NOT HAVE AN OBLIGATION TO CONDUCT BACKGROUND CHECKS ON ANY MEMBER, INCLUDING, BUT NOT LIMITED TO, SEEKERS AND EXPERTS, BUT MAY CONDUCT SUCH BACKGROUND CHECKS IN ITS SOLE DISCRETION. THE SITE, APPLICATION, SERVICES AND COLLECTIVE CONTENT ARE PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED. WITHOUT LIMITING THE FOREGOING, CLARITY EXPLICITLY DISCLAIMS ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT, AND ANY WARRANTIES ARISING OUT OF COURSE OF DEALING OR USAGE OF TRADE. CLARITY MAKES NO WARRANTY THAT THE SITE, APPLICATION, SERVICES, COLLECTIVE CONTENT, INCLUDING, BUT NOT LIMITED TO, THE LISTINGS OR ANY INFORMATION PROVIDED VIA APPOINTMENTS WILL MEET YOUR REQUIREMENTS OR BE AVAILABLE ON AN UNINTERRUPTED, SECURE, OR ERROR-FREE BASIS. CLARITY MAKES NO WARRANTY REGARDING THE QUALITY OF ANY LISTINGS, THE SERVICES OR COLLECTIVE CONTENT OR THE ACCURACY, TIMELINESS, TRUTHFULNESS, COMPLETENESS OR RELIABILITY OF ANY COLLECTIVE CONTENT OBTAINED THROUGH THE SITE, APPLICATION OR THE SERVICES.
          </p>
          <p>
            NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED FROM CLARITY OR THROUGH THE SITE, APPLICATION, SERVICES OR COLLECTIVE CONTENT, WILL CREATE ANY WARRANTY NOT EXPRESSLY MADE HEREIN.
          </p>
          <p>YOU ARE SOLELY RESPONSIBLE FOR ALL OF YOUR COMMUNICATIONS AND INTERACTIONS WITH OTHER USERS OF THE SITE, APPLICATION OR SERVICES AND WITH OTHER PERSONS WITH WHOM YOU COMMUNICATE OR INTERACT AS A RESULT OF YOUR USE OF THE SITE, APPLICATION OR SERVICES. YOU UNDERSTAND THAT CLARITY DOES NOT MAKE ANY ATTEMPT TO VERIFY THE STATEMENTS OF USERS OF THE SITE, APPLICATION OR SERVICES. CLARITY MAKES NO REPRESENTATIONS OR WARRANTIES AS TO THE CONDUCT OF USERS OF THE SITE, APPLICATION OR SERVICES OR THEIR COMPATIBILITY WITH ANY CURRENT OR FUTURE USERS OF THE SITE, APPLICATION OR SERVICES. YOU AGREE TO TAKE REASONABLE PRECAUTIONS IN ALL COMMUNICATIONS AND INTERACTIONS WITH OTHER USERS OF THE SITE, APPLICATION OR SERVICES AND WITH OTHER PERSONS WITH WHOM YOU COMMUNICATE OR INTERACT AS A RESULT OF YOUR USE OF THE SITE, APPLICATION OR SERVICES, INCLUDING, BUT NOT LIMITED TO, EXPERTS, REGARDLESS OF WHETHER SUCH COMMUNICATIONS OR INTERACTIONS ARE ORGANIZED BY CLARITY.
          </p>

          <h3>Limitation of Liability</h3>

          <p>
            YOU ACKNOWLEDGE AND AGREE THAT, TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE ENTIRE RISK ARISING OUT OF YOUR ACCESS TO AND USE OF THE SITE, APPLICATION, SERVICES AND COLLECTIVE CONTENT, LISTINGS OR APPOINTMENTS VIA THE SITE, APPLICATION AND SERVICES, AND ANY CONTACT YOU HAVE WITH OTHER USERS OF CLARITY WHETHER IN PERSON, BY PHONE, ONLINE OR OTHER MEANS REMAINS WITH YOU. NEITHER CLARITY NOR ANY OTHER party involved in creating, producing, or delivering the Site, APPLICATION, Services OR COLLECTIVE CONTENT will be liable for any incidental, special, exemplary or consequential damages, INCLUDING LOST PROFITS, LOSS OF DATA OR LOSS OF GOODWILL, SERVICE INTERRUPTION, COMPUTER DAMAGE OR SYSTEM FAILURE OR THE COST OF SUBSTITUTE PRODUCTS OR SERVICES, OR FOR ANY DAMAGES FOR PERSONAL OR BODILY INJURY OR EMOTIONAL DISTRESS arising ouT of or in connection with thESE TERMS, from the use OF or inability to use the SITE, APPLICATION, ServiceS OR COLLECTIVE CONTENT, FROM ANY COMMUNICATIONS, INTERACTIONS OR MEETINGS WITH OTHER USERS OF THE SITE, APPLICATION, OR SERVICES OR OTHER PERSONS WITH WHOM YOU COMMUNICATE OR INTERACT AS A RESULT OF YOUR USE OF THE SITE, APPLICATION, SERVICES, whether based on warranty, contract, tort (including negligence), PRODUCT LIABILITY or any other legal theory, and whether or not CLARITY has been informed of the possibility of such damage, EVEN IF A limited REMEDY SET FORTH HEREIN IS FOUND TO HAVE FAILED OF ITS ESSENTIAL PUrPOSE.
          </p>
          <p>
          IN no event will clarity’s aggregate liability arising out of or in connection with THESE TERMS AND YOUR USE OF THE SITE, APPLICATION AND SERVICES INCLUDING, BUT NOT LIMITED TO, FROM YOUR LISTING OR BOOKING OF ANY appointment VIA THE SITE, APPLICATION AND SERVICES, OR FROM THE USE OF OR INABILITY TO USE THE SITE, APPLICATION, SERVICES OR COLLECTIVE CONTENT AND IN CONNECTION WITH ANY INTERACTIONS WITH ANY OTHER MEMBERS, EXCEED the AMOUNTS YOU HAVE PAID OR OWE FOR appointments made VIA THE SITE, APPLICATION AND SERVICES AS A member IN THE TWELVE (12) MONTH PERIOD PRIOR TO THE EVENT GIVING RISE TO THE LIABILITY or ONE HUNDRED DOLLARS ($100), IF NO SUCH PAYMENTS HAVE BEEN MADE, AS APPLICABLE. THE LIMITATIONS OF DAMAGES SET FORTH ABOVE ARE FUNDAMENTAL ELEMENTS OF THE BASIS OF THE BARGAIN BETWEEN clarity AND YOU. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, SO THE ABOVE LIMITATION MAY NOT APPLY TO YOU.
          </p>

          <h3>Indemnification</h3>

          <p>You agree to release, indemnify, and hold Clarity and its affiliates and subsidiaries, and their officers, directors, employees and agents, harmless from and against any claims, liabilities, damages, losses, and expenses, including, without limitation, reasonable legal and accounting fees, arising out of or in any way connected with (a) your access to or use of the Site, Application, Services, or Collective Content or your violation of these Terms; (b) your Member Content and (c) your (i) interaction with any Member, (ii) reliance on any information exchanged via the Site, Application or Services, or (iii) creation of a Listing.  Clarity shall have the right to control all defense and settlement activities.
          </p>

          <h3>Export Control and Restricted Countries</h3>

          <p>
            You may not use, export, re-export, import, or transfer the Application except as authorized by United States law, the laws of the jurisdiction in which you obtained the Application, and any other applicable laws. In particular, but without limitation, the Application may not be exported or re-exported: (a) into any United States embargoed countries; or (b) to anyone on the U.S. Treasury Department’s list of Specially Designated Nationals or the U.S. Department of Commerce’s Denied Person’s List or Entity List. By using the Site, Application and Services, you represent and warrant that (i) you are not located in a country that is subject to a U.S. Government embargo, or that has been designated by the U.S. Government as a “terrorist supporting” country and (ii) you are not listed on any U.S. Government list of prohibited or restricted parties. You also will not use the Site, Application and Services for any purpose prohibited by U.S. law, including the development, design, manufacture or production of missiles, or nuclear, chemical or biological weapons. Clarity does not permit Listings associated with certain countries due to U.S. embargo restrictions.
          </p>

          <h3>Feedback and Reporting Misconduct</h3>

          <p>We welcome and encourage you to provide feedback, comments and suggestions for improvements to the Site, Application and Services (<strong>“Feedback”</strong>). You may submit Feedback by emailing us at <a href="mailto:support@clarity.fm" class="allow-default">support@clarity.fm</a>. You acknowledge and agree that all Feedback will be the sole and exclusive property of Clarity and you hereby irrevocably assign to Clarity and agree to irrevocably assign to Clarity all of your right, title, and interest in and to all Feedback, including without limitation all worldwide patent, copyright, trade secret, moral and other proprietary or intellectual property rights therein. At Clarity’s request and expense, you will execute documents and take such further acts as Clarity may reasonably request to assist Clarity to acquire, perfect, and maintain its intellectual property rights and other legal protections for the Feedback.
          </p>
          <p>In addition, If you feel any user is acting or has acted inappropriately, including but not limited to, anyone who (i) engages in offensive, violent or sexually inappropriate behavior, (ii) you suspect of fraud, or (iii) engages in any other disturbing conduct, you should immediately report such person to the appropriate authorities and to Clarity.
          </p>

          <h3>Assignment</h3>

          <p>You may not assign or transfer these Terms, by operation of law or otherwise, without Clarity’s prior written consent. Any attempt by you to assign or transfer these Terms, without such consent, will be null and of no effect. Clarity may assign or transfer these Terms, at its sole discretion, without restriction. Subject to the foregoing, these Terms will bind and inure to the benefit of the parties, their successors and permitted assigns.
          </p>

          <h3>Notices</h3>

          <p>
            Unless otherwise specified herein, any notices or other communications permitted or required hereunder, including those regarding modifications to these Terms, will be in writing and given by Clarity (i) via email (in each case to the email address that you provide) or (ii) by posting to the Site or via the Application. For notices made by e-mail, the date of receipt will be deemed the date on which such notice is transmitted.
          </p>

          <h3>Controlling Law and Jurisdiction</h3>

          <p>These Terms will be interpreted in accordance with the laws of the State of California and the United States of America, without regard to its conflict-of-law provisions. You and we agree to submit to the personal jurisdiction of a state court located in San Francisco, California for any actions for which the parties retain the right to seek injunctive or other equitable relief in a court of competent jurisdiction to prevent the actual or threatened infringement, misappropriation or violation of a party’s copyrights, trademarks, trade secrets, patents, or other intellectual property rights, as set forth in the Dispute Resolution provision below.
          </p>

          <h3>Severability</h3>

          <p>These Terms are intended to govern the agreement between Clarity and you to the extent permitted by all applicable laws, ordinances, rules, and regulations.  If any provision of these Terms or the application thereof to any person or circumstances shall, for any reason or to any extent, be invalid or unenforceable, the remainder of these Terms and the application of such provision to other persons or circumstances shall not be affected thereby, but rather shall be enforced to the greatest extent permitted by law.
          </p>

          <h3>Dispute Resolution</h3>

          <p>
            You and Clarity agree that any dispute, claim or controversy arising out of or relating to these Terms or the breach, termination, enforcement, interpretation or validity thereof, or to the use of the Services or use of the Site or Application (collectively, “Disputes”) will be settled by binding arbitration , except that each party retains the right to seek injunctive or other equitable relief in a court of competent jurisdiction to prevent the actual or threatened infringement, misappropriation or violation of a party’s copyrights, trademarks, trade secrets, patents, or other intellectual property rights. You acknowledge and agree that you and Clarity are each waiving the right to a trial by jury or to participate as a plaintiff or class member in any purported class action or representative proceeding. Further, unless both you and Clarity otherwise agree in writing, the arbitrator may not consolidate more than one person’s claims, and may not otherwise preside over any form of any class or representative proceeding. If this specific paragraph is held unenforceable, then the entirety of this “Dispute Resolution” section will be deemed void. Except as provided in the preceding sentence, this “Dispute Resolution” section will survive any termination of these Terms.
            </p>
            <p>
            <em>Arbitration Rules and Governing Law.</em> The arbitration will be administered by the American Arbitration Association (“AAA”) in accordance with the Commercial Arbitration Rules and the Supplementary Procedures for Consumer Related Disputes (the “AAA Rules”) then in effect, except as modified by this “Dispute Resolution” section. (The AAA Rules are available at <a href="http://www.adr.org/arb_med" class="allow-default">www.adr.org/arb_med</a> or by calling the AAA at 1-800-778-7879.) The Federal Arbitration Act will govern the interpretation and enforcement of this section.
            </p>
            <p>
            <em>Arbitration Process.</em> A party who desires to initiate arbitration must provide the other party with a written Demand for Arbitration as specified in the AAA Rules. (The AAA provides a form Demand for Arbitration at <a href="http://www.adr.org/si.asp?id=3477" class="allow-default">www.adr.org/si.asp?id=3477</a> and a separate form for California residents at <a href="http://www.adr.org/si.asp?id=3485" class="allow-default">www.adr.org/si.asp?id=3485</a>.) The arbitrator will be either a retired judge or an attorney licensed to practice law in the state of California and will be selected by the parties from the AAA’s roster of consumer dispute arbitrators. If the parties are unable to agree upon an arbitrator within seven (7) days of delivery of the Demand for Arbitration, then the AAA will appoint the arbitrator in accordance with the AAA Rules.
            </p>
            <p>
            <em>Arbitration Location and Procedure.</em> Unless you and Clarity otherwise agree, the arbitration will be conducted in the county where you reside. If your claim does not exceed $10,000, then the arbitration will be conducted solely on the basis of documents you and Clarity submit to the arbitrator, unless you request a hearing or the arbitrator determines that a hearing is necessary. If your claim exceeds $10,000, your right to a hearing will be determined by the AAA Rules. Subject to the AAA Rules, the arbitrator will have the discretion to direct a reasonable exchange of information by the parties, consistent with the expedited nature of the arbitration.
            </p>
            <p>
            <em>Arbitrator’s Decision.</em> The arbitrator will render an award within the time frame specified in the AAA Rules. The arbitrator’s decision will include the essential findings and conclusions upon which the arbitrator based the award. Judgment on the arbitration award may be entered in any court having jurisdiction thereof. The arbitrator’s award damages must be consistent with the terms of the “Limitation of Liability” section above as to the types and the amounts of damages for which a party may be held liable. The arbitrator may award declaratory or injunctive relief only in favor of the claimant and only to the extent necessary to provide relief warranted by the claimant’s individual claim. If you prevail in arbitration you will be entitled to an award of attorneys’ fees and expenses, to the extent provided under applicable law.
            </p>
            <p>
            <em>Fees.</em> Your responsibility to pay any AAA filing, administrative and arbitrator fees will be solely as set forth in the AAA Rules. However, if your claim for damages does not exceed $75,000, Clarity will pay all such fees unless the arbitrator finds that either the substance of your claim or the relief sought in your Demand for Arbitration was frivolous or was brought for an improper purpose (as measured by the standards set forth in Federal Rule of Civil Procedure 11(b)).
            </p>
            <p>
            <em>Changes.</em> Notwithstanding the provisions of the “Modification” section above, if Clarity amends this “Dispute Resolution” section after the date you first accepted these Terms (or accepted any subsequent changes to these Terms) you will be notified in accordance with these Terms.  You may reject any such change by sending us written notice (including by email to <a href="mailto:support@clarity.fm" class="allow-default">support@clarity.fm</a>) within 30 days of the date such change became effective, as indicated in the “Last Updated Date” above or in the date of Clarity’s email to you notifying you of such change. By rejecting any change, you are agreeing that you will arbitrate any Dispute between you and Clarity in accordance with the provisions of this “Dispute Resolution” section as of the date you first accepted these Terms (or accepted any subsequent changes to these Terms).
            </p>

            <h3>Entire Agreement</h3>

            <p>These Terms constitute the entire and exclusive understanding and agreement between Clarity and you regarding the Site, Application, Services, Collective Content and any Appointments or Listings made via the Site, Application and Services, and these Terms supersede and replace any and all prior oral or written understandings or agreements between Clarity and you regarding the same.
            </p>

            <h3>General</h3>

            <p>The failure of Clarity to enforce any right or provision of these Terms will not constitute a waiver of future enforcement of that right or provision. The waiver of any such right or provision will be effective only if in writing and signed by a duly authorized representative of Clarity. Except as expressly set forth in these Terms, the exercise by either party of any of its remedies under these Terms will be without prejudice to its other remedies under these Terms or otherwise. If for any reason an arbitrator or a court of competent jurisdiction finds any provision of these Terms invalid or unenforceable, that provision will be enforced to the maximum extent permissible and the other provisions of these Terms will remain in full force and effect.</p>

            <h3>Contacting Clarity</h3>

            <p>If you have any questions about these Terms, please contact Clarity at:</p>

            <p>
              </p><table class="address">
                <tbody><tr>
                  <th>Mail:</th>
                  <td>
                    Fundable LLC<br>
                    Customer Care - Privacy Policy Issues<br>
                    1322 Manning Parkway<br>
                    Powell, OH 43065
                  </td>
                </tr>
                <tr>
                  <th>Email:</th>
                  <td>
                    <a href="mailto:support@clarity.fm" class="allow-default">support@clarity.fm</a>
                  </td>
                </tr>
              </tbody></table>
            <p></p>

    <h2>Privacy Policy</h2>
    <hr>

      <p>Clarity is owned and operated by Fundable LLC (hereinafter collectively referred to as <strong>“Clarity”</strong>, <strong>“we”</strong> or <strong>“us”</strong>). Clarity takes your privacy very seriously.  We provide this Privacy Policy to inform you of our policies and procedures regarding the collection, use and disclosure of personal information we receive from users of www.clarity.fm (the <strong>"Site"</strong>) and our application for mobile devices (the <strong>"Application"</strong>).
      Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms of Service (<a href="http://www.clarity.fm/terms" class="allow-default">www.clarity.fm/terms</a>).
      </p>
      <h3>Scope</h3>
      <p>This Privacy Policy, as amended from time to time, applies only to information that you provide to us through the Site and Application. Please note that the Site and Application may contain links to other websites. If you click on a link to another website, this Privacy Policy will not apply to any information collected on that website. Clarity is not responsible for the privacy practices, security, or content of these other websites, and we recommend that you read the privacy policies of each website that you visit.
      </p>
      <p>
      As used in this policy, the terms <strong>"using"</strong> and <strong>"processing"</strong> information include using cookies on a computer, subjecting the information to statistical or other analysis and using or handling information in any way, including, but not limited to collecting, storing, evaluating, modifying, deleting, using, combining, disclosing and transferring information within our organization or among our affiliates within the United States or internationally.
      </p>

      <h3>The Collection of Information</h3>

      <p>Our principal goals in collecting information are to provide and improve our Site, Application, services, features and content, to administer your use of the Site and Application (collectively, the <strong>"Service"</strong>) and to enable users to enjoy and easily navigate the Site and Application. The types of information that we collect include:
      </p>
      <p>
      Information you provide to us
      </p>
      <p>

      <em>Personal Information:</em> You may browse the Site and Application without providing any personal information that may be used to identify you personally.  However, when you register for and use the Service, access certain content or features, or directly contact the Site or Application, we may ask you to provide information, including:
      </p>
      <p>
        </p><ul>
          <li>your name, phone number, mobile phone number, email address and postal address;</li>
          <li>your username, password, and account setting preferences;</li>
          <li>your credit card number, billing address and other billing related information.</li>
        </ul>
      <p></p>
      <p>
      (collectively, <strong>“Personal Information”</strong>)
      Information automatically collected
      When you visit and interact on the Site or Application, certain information is collected automatically, including:
      </p>
      <p>
      <em>Non-Identifying Information:</em>  We collect other non-identifying information you provide as part of the registration, administration and personalization of your Clarity Account profile (e.g. account preferences) (<strong>“Non-Identifying Information”</strong>). We may combine your Personal Information with Non-Identifying Information and aggregate it with information collected from other Clarity Users (defined below) to attempt to provide you with a better experience, to improve the quality and value of the Site, Application and Service and to analyze and understand how our Site, Application and Service are used. We may also use the combined information without aggregating it to serve you specifically, for instance to deliver a product to you according to your preferences or restrictions.
       </p>
        <p>
      <em>Log Data:</em>  When you visit the Site and Application, whether as a Member or a non-registered user (any of these, a <strong>"Clarity User"</strong>), our servers automatically record information that your browser sends whenever you visit a website (<strong>"Log Data"</strong>). This Log Data may include information such as your computer’s Internet Protocol (<strong>"IP"</strong>) address, browser type or the webpage you were visiting before you came to our Site and Application, pages of our Site and Application that you visit, the time spent on those pages, information you search for on our Site and Application, access times and dates, and other statistics. We use this information to monitor and analyze use of the Site, Application and the Service and for the Site and Application’s technical administration, to increase our Site and Application’s functionality and user-friendliness, and to better tailor our Site and Application to our visitors’ needs. We do not treat Log Data as Personal Information or use it in association with other Personal Information, though we may aggregate, analyze and evaluate such information for the same purposes as stated above regarding other Non-Identifying Information.

      </p>
      <p>
      <em>Cookies:</em> Like many websites, we use "cookies" to collect information. A cookie is a small data file that we transfer to your computer’s hard disk for record-keeping purposes. We use cookies for two purposes. First, we utilize persistent cookies to save your login information for future logins to the Site and Application. Second, we utilize session cookies to enable certain features of the Site and Application, to better understand how you interact with the Site and Application and to monitor aggregate usage by Clarity Users and web traffic routing on the Site and Application. Unlike persistent cookies, session cookies are deleted from your computer when you log off from the Site or Application and then close your browser. You can instruct your browser, by changing its options, to stop accepting cookies or to prompt you before accepting a cookie from the websites you visit. If you do not accept cookies, however, you may not be able to use all portions of the Site or Application or all functionality of the Service.
      </p>
      <p>
      <em>Web Beacons:</em> Our Site and Application may contain electronic images known as Web beacons (sometimes called single-pixel gifs) and are used along with cookies to compile aggregated statistics to analyze how our Site and Application are used and may be used in some of our emails to let us know which emails and links have been opened by recipients. This allows us to gauge the effectiveness of our Member communications and marketing campaigns.
      </p>
      <p>
      </p><ul>
        <li>(c) Information collected from other sources</li>
      </ul>
      <p>
      We may access information about you from third-party sources and platforms.  You can register to use the Service by logging into online accounts you may have with third party service providers (<strong>"TPSP"</strong>)(e.g. Facebook, Twitter or LinkedIn); each such account, a <strong>"Third Party Account"</strong>, via our Site or Application as described below. As part of the functionality of the Site, Service and Application, you may link your Clarity Account with Third Party Accounts, by either: (i) providing your Third Party Account login information to Clarity through the Site, Service or Application; or (ii) allowing Clarity to access your Third Party Account, as is permitted under the applicable terms and conditions that govern your use of each Third Party Account. You represent that you are entitled to disclose your Third Party Account login information to Clarity and/or grant Clarity access to your Third Party Account for use for the purposes described herein without breach by you of any of the terms and conditions that govern your use of the applicable Third Party Account and without obligating Clarity to pay any fees or making Clarity subject to any usage limitations imposed by the applicable TPSP.
      </p>
      <p>
      If you register by logging into a Third Party Account via our Site or Application, we will obtain the Personal Information you have provided to the applicable TPSP (such as your true name, email address, profile picture, names of TPSP friends, names of TPSP groups to which you belong, other information you make publicly available via the applicable TPSP and/or other information you authorize Clarity to access by authorizing the TPSP to provide such information) from your Third Party Accounts and use that information to create your Clarity Account and Clarity Account profile page. Depending on the Third Party Accounts you choose and subject to the privacy settings that you have set in such Third Party Accounts, you understand that by granting us access to the Third Party Accounts, we will access, make available and store (if applicable and as permitted by the TPSP and authorized by you) the information in your Third Party Accounts so that it is available on and through your Clarity Account on the Site, Service and Application. If there is information about your "friends" or people with whom you are associated in your Third Party Account, the information we obtain about those "friends" or people with whom you are associated, may also depend on the privacy settings such people have with the applicable TPSP.
      </p>

      <h3>Use of Your Information</h3>

      <p>
      We do not use Personal Information for purposes other than those disclosed to you in this Privacy Policy and/or at the time it was collected, except with your consent or as required by law. We collect information for one or more of the following purposes:
      </p>
      <p>
      <em>Email Communications:</em> We may use the information that we collect to send you email communications or information about your account or changes to the Site, Application or Service (including changes to the Terms of Service and this Privacy Policy).
      </p>
      <p>
      <em>Mobile Communications:</em> With your consent, we may use your mobile phone number to send you information, notifications and updates regarding the Site, Application or Service.
      </p>
      <p>
      <em>Profile Information:</em> We use the information we collect at registration to create your Clarity Account and Clarity Account profile page. Your Clarity Account profile page will include, among other things, your first and last name, your profile picture and your location. You can select the other items of Personal Information that you wish to be included in your Clarity Account profile page – including, but not limited to, a list of the TPSP groups to which you belong, a list of any of your TPSP friends or connections who are also Members of Clarity, connections you have established between your Clarity Account and any TPSPs, and a biography and links to your Listings, if applicable (together, your <strong>"Profile Information"</strong>). We will display your Profile Information in your Clarity Account profile page publicly via the Site and Application, and, with your prior permission, on third party sites. Any information you choose to provide as part of your Profile Information will be publicly visible to all Clarity Users and consequently should reflect how much you want other Clarity Users to know about you. We recommend that you guard your anonymity and sensitive information and we encourage Members to exercise caution regarding the information disclosed in their Clarity Account profile page. You can review and edit your Profile Information at any time.
      </p>
      <p>
      <em>Listings:</em> If you create a Listing, we may publish, use, share or otherwise disclose the Content of that Listing publicly via the Site and Application and may enable third parties to publish the Listing on their websites.

      </p>
      <p>
      <em>Clarity Marketing:</em> We may also use your Personal Information to contact you with Clarity newsletters, marketing or promotional materials and other information that may be of interest to you. You may opt out of these marketing related notifications at any time by accessing the “Manage Notifications” section of your Clarity Account.  Please note that we may also use your Personal Information to contact you with information related to your use of the Service; you may not opt out of these notifications.
      </p>
      <p>
      <em>Request Fulfillment:</em> We may use the information that we collect to fulfill your requests for products, services and information.  For example, we may use your information to respond to your customer service requests.
      </p>
      <p>
      <em>Data Analysis:</em> In order to learn more about how our Site, Application and Service are used, we aggregate and analyze the data we collect.  We may use the information, for example, to monitor and analyze use of the Site, Application and Service, to improve the functionality of same and to better tailor our content and design to suit our visitors’ needs.
      </p>
      <p>
      <em>Testimonials:</em> Clarity posts testimonials on the Site and Application.  With your consent, we may post your testimonial on the Site and Application along with your name.  To have your testimonial removed, please contact us at <a href="mailto:support@clarity.fm" class="allow-default">support@clarity.fm</a>.
      </p>


      <h3>Information Sharing and Disclosure</h3>

      <p>
      We do not share or disclose Personal Information for purposes other than those disclosed to you in this Privacy Policy and/or at the time it was collected, except with your consent or as required by law. We will make information about you available to other companies, applications, or people in the circumstances listed below:
      </p>
      <p>
      <em>Analysis:</em> We may share aggregated information that does not include Personal Information and we may otherwise disclose Non-Identifying Information and Log Data with third parties for industry analysis, demographic profiling, payment processing, customer service and other purposes. Any aggregated information shared in these contexts will not contain your Personal Information.
      </p>
      <p>
      <em>Service Delivery:</em> We may employ third party companies and individuals to perform Site-related services, including maintenance services, database management, web analytics, data processing and email and text message distribution.  These third parties have access to your information only to perform these tasks on our behalf.
      </p>
      <p>
      <em>Corporate Transaction:</em> We may share information about you in the event that the Site is acquired by or merged with another company or a similar corporate transaction takes place. However, we will notify you by placing a prominent notice on the Site or sending a notice to the primary email address specified in your account before your information is transferred and becomes subject to a different privacy policy.
      </p>
      <p>
      <em>Investigations:</em> We may share information about you to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety or other rights and interests of any person, violations of Clarity’s Terms of Service, or as otherwise required by law.
      </p>
      <p>
      <em>Compliance with Laws:</em> We may share information about you to respond to subpoenas, search warrants, judicial proceedings, court orders, legal process, or other law enforcement measures, to establish or exercise our legal rights, or to defend against legal claims.
      </p>


      <h3>Third Party Services</h3>

      <p>
      To customize your experience on the Site and Application and to simplify our registration process, we provide you with the opportunity to access or interact with TPSPs, such as Facebook, Twitter and LinkedIn. When you connect to the Site or Application through these TPSPs, we may share information about you with these third-party service providers and they may share data about you with us.
      </p>
      <p>
      When you allow us to access your data through a third-party service to create a Site account, we may use this data for several purposes, including:
      </p>
      <p>
      Creating relationships automatically within our system. For example, if you connect to us via a service with a public friend list, like Twitter, we may check to see if any people you follow on Twitter are also Site members. If web find a match, we will replicate your Twitter relationship with those members, setting them to be fans, followers, or friends on our Site.
      </p>
      <p>
      Populating a list of potential friends to whom you can send service-specific messages. For example, we may use friend lists from a TPSP to create a list of contacts to whom you may choose to share your Clarity Account profile page.
      </p>
      <p>
      To enhance and personalize your experience on the Site. When you are connected via a TPSP, we may access certain account information, such as your profile picture, in order to enhance and personalize your experience on the Site.
      </p>
      <p>
      Please remember that we do not control the privacy practices of these third-party services. We encourage you to read the privacy policies of all TPSP websites.
      </p>

      <h3>Updating and Deleting your Personal Information</h3>

      <p>
      All Members may review, update, correct or delete the Personal Information in their registration profile by contacting us at <a href="mailto:support@clarity.fm" class="allow-default">support@clarity.fm</a> or editing their profile. If you would like us to deactivate your Clarity Account, please contact us or select the "Cancel Account" feature of the Service and we will attempt to accommodate your request if we do not have any legal obligation or a legitimate business reason to retain the information contained in your Clarity Account. Please note that, if you cancel your Clarity Account, any of your Member Content on the Site and Application will remain publicly viewable via the Site and Application. Please see below for privacy contact information
      </p>

      <h3>Security and Data Retention</h3>

      <p>
      To protect your privacy and information, Clarity uses multiple security procedures and practices to protect from unauthorized access, destruction, use, modification and disclosure of Personal Information of users.
      </p>
      <p>
      When you enter Personal Information on our Site and Application, we encrypt that information using secure socket layer technology (SSL). SSL creates a secured connection between our web servers and your browser, which protects against unauthorized access to transmitted data and supports data being sent only to intended recipients.
      </p>
      <p>
      Your Personal Information is password protected and our main servers are locked and hosted by a leading provider of Internet access to enterprises with mission-critical Internet application requirements. Access to the host environment is secure.
      </p>
      <p>
      Identity theft and the practice currently known as "phishing" are of great concern to Clarity. Safeguarding information to help protect you from identity theft is a top priority. We do not and will not, at any time, request your credit card information, your Clarity Account ID, login password, or national identification numbers in a non-secure or unsolicited email or telephone communication.
      </p>
      <p>
      We follow generally accepted industry standards to protect the Personal Information submitted to us, both during transmission and once we receive it. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure. Therefore, we cannot guarantee its absolute security. If you have any questions about security on our Site and Application, you can contact us at <a href="mailto:support@clarity.fm" class="allow-default">support@clarity.fm</a>.
      </p>
      <p>
      Note that we will make any legally required disclosures of any breach of the security, confidentiality, or integrity of your unencrypted electronically stored "personal data" (as defined in applicable state statutes on security breach notification) to you via email or conspicuous posting on the Site and Application in the most expedient time possible and without unreasonable delay, insofar as consistent with (i) the legitimate needs of law enforcement or (ii) any measures necessary to determine the scope of the breach and restore the reasonable integrity of the data system.
      </p>

      <p>
        Sharing personal email addresses, phone numbers, Skype/IM usernames or any other personal contact details.
      </p>

      <p>
        All communications, information and files exchanged should be performed exclusively using Clarity’s messaging system. The main reason this it is required is to keep communications and transactions on Clarity is for your safety. The internet industry has traditionally been inundated with risk and uncertainty. Clarity is a new form of communications. For this community to thrive, security and trust must be maintained for all users. This is only possible through maintaining initial conversations within the Clarity application. Protecting our users’ privacy is of critical concern for everyone on the Clarity team. To protect our users’ privacy, personal information should be kept anonymous. Requesting or providing email addresses, skype/IM usernames or other personal contact details (other than your name) are strictly prohibited. Clarity’s Call Back service and User Inboxes are designed to enable communication between all users.
      </p>

      <h3>International Transfer</h3>
      <p>Clarity and its third party partners to whom we disclose information under this policy may perform activities outside of the United States and potentially collect, transfer, store, and process your personal information in other countries. As a result, your information may be subject to the laws of those countries or other jurisdictions. These laws may not be equivalent to the privacy and data protection laws in your jurisdiction. Information may be disclosed in response to valid demands or requests from government authorities, courts, or law enforcement in these countries. By using the Site, Application or providing us with your information, you consent to the potential collection, transfer, storage, and processing of information in other countries.
      </p>


      <h3>Access</h3>
      <p>
      We will make available to you any of your personal information that we have collected, used or disclosed upon request.  Please contact us at support@cl arity.fm.
      </p>

      <h3>Updates to This Policy</h3>

      <p>By using the Site, Application and Service you are accepting the terms of this Privacy Policy, as it may be amended from time to time. Please check the date of this Policy each time you visit to ensure that you are aware of the most current version. If you do not agree to the terms of this Policy, please do not use this Site, Application and Service.
      </p>
      <p>
      We may update this Privacy Policy to reflect changes to our information practices. If we make any material changes we will notify you by email (sent to the e-mail address specified in your Clarity Account) or by means of a notice on the Site and Application prior to the change becoming effective. We encourage you to periodically review this page for the latest information on our privacy practices.
      </p>

      <h3>Addressing Your Concerns</h3>

      <p>
      We are accountable for our compliance with this Privacy Policy and the Personal Information under our control. We have implemented policies and practices to give effect to this policy, including training and communicating with our employees the importance of privacy. We regularly monitor our procedures and security measures to ensure that they remain effective and that they are being properly administered. If you have questions or suggestions or wish to contact our Privacy Officer, please complete this feedback form or you can contact us at:
      </p>
      <p>
        </p><table class="address">
          <tbody><tr>
            <th>Mail:</th>
            <td>
              Fundable LLC<br>
              Customer Care - Privacy Policy Issues<br>
              1322 Manning Parkway<br>
              Powell, OH 43065
            </td>
          </tr>
          <tr>
            <th>Email:</th>
            <td><a href="mailto:support@clarity.fm" class="allow-default">support@clarity.fm</a></td>
          </tr>
        </tbody></table>
      <p></p>

      <hr>

      <h3>Data Rights and Protection</h3>

      <p>Startups.com has the utmost respect for our customers’ privacy and information protection. Given the ever changing regulatory environment, we strive to stay ahead of the curve. In our mission to ethically and lawfully serve our customers we have the following policies in place in order to adhere to international and domestic law:</p>

      <h4>Obtaining Consent:</h4>

      <p>Startups.com wrote our terms of service to make it as understandable and straightforward as possible. Our valued customers are able to give and rescind consent at any time. If at any time you wish to remove consent, please email our Data Protection Officer at <a href="mailto:data@startups.com" class="allow-default">data@startups.com</a>. Additional details are provided below in our “Data Protection Officer” description. </p>

      <h4>Timely Breach Notification:</h4>

      <p>In the event of a data breach, Startups.com will notify our associated data controllers and customers within 72 hours. In addition to outlining the nature of the breach, the breadth, and actions involved to remedy the situation will be detailed. </p>

      <h4>Right to Data Access: </h4>

      <p>If at any time a customer wishes to access his or her existing data profile, Startups.com will provide a free electronic copy of the data we collected about that customer. This report will also include the various ways the information has been used.</p>

      <h4>Right to Be Forgotten: </h4>

      <p>If at any time customer discontinues their relationship with Startups.com, the customer can request that his or her personal data is wholly erased from our records. </p>

      <h4>Data Portability:</h4>

      <p>This gives users rights to their own data. Customers will be able to obtain their data from Startups.com in an electronic report and reuse that same data in different environments outside of ours.</p>


      <h4>Privacy by Design:</h4>


      <p>Startups.com has a full and detailed map of our data collection process and the various parties privy to that data. Startups.com has specifically designed its systems and trained its staff to maintain customer privacy. We strive to continue to improve and adapt this design to be forward looking.</p>


      <h4>Data Protection Officer:</h4>


      <p>Startups.com has appointed a Data Protection Officer who can be reached at <a href="mailto:data@startups.com" class="allow-default">data@startups.com</a>. This internal officer will be able to rescind customer consent as requested, alert stakeholders of a data breach, provide access to data reports, ensure the right to be forgotten, establish data portability as requested, and oversee privacy by design with a fiduciary responsibility. </p>
                </div>
            </div>
        </div>
        <div class="contbtm">
        	<div class="container">
            	<h3>Powell, OH</h3>
                <p>1322 Manning Parkway <br>Powell, OH 43065</p>
                <h3>Help</h3>
                <p>Need Help? Feel free to email us your question directly or setup an appointment for phone support by emailing <a href="#" >support@clarity.fm</a>. You can also checkout our <a href="#">Help Center</a> to see if we've already answered your question.</p>
                <h3>Press Inquiries</h3>
                <p>If you’re working on a piece about Clarity, please feel free to <a href="#">email us</a> directly. We can provide background information about the company and availability for our team members regarding speaking opportunities.</p>
                <p><a href="#">Download Media Kit</a></p>
            </div>
        </div>
    </section>
@endsection