@extends('layouts.app')

@section('content')
  <section>
    	<div class="bnr bnr-sub">
        	<div class="container">
            	<a href="">Back</a>
            </div>
        </div>
    	<div class="bnr2 m-0 border-top-0">
        	<div class="container">
            	<div class="toptab3s">
                	<h1>Help Center</h1>
                 </div>
            </div>
        </div>
        <div class="content-sec">
        	<div class="container">
                <div class="experts2s">
                	<div class="row">
                    	<div class="col-lg-8 col-md-8 col-sm-12 float-left">
                        	<h2>How do I edit my profile?</h2>
                            <p>You can edit your profile by going to your <a href="#">Account</a> > <a href="#">Edit Profile.</a></p>
                            <p>A great profile includes your full name, a <a href="#">profile photo</a> with your lovely face, and some interesting facts about your current and past experiences. This is your opportunity to highlight how amazing you are, so feel free to be creative!</p>
                            <p><a href="#">Here's an example of a great profile</a></p>
                            <img src="images/img5s.png" alt="">
                            <p>You can share any company website, blog, or links that relate to your <a href="#">expertise</a> as well as any other online accounts you'd like to share that showcase your experience.</p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 float-left ansform">
                        	<form>
                                <input type="text" placeholder="Search">
                                <input type="submit" value="&#xf002" class="fa">
                            </form>
                            <div class="btmright">
                            	<h3>Can't find what you're looking for?</h3>
                                <p><a href="#">Send us an email.</a> Phone support is available by appointment only. Please email us your name, phone number, problem/issue and availability Mon - Fri 9am - 5pm EDT.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection