@extends('layouts.app')

@section('content')
    <search :search_results="{{ json_encode($search_results) }}" :expertise_images="{{$expertise_images}}"></search>
@endsection
