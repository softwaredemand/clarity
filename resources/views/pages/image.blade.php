@extends('layouts.app')

@section('content')
<profile-image :user="{{ json_encode($User) }}"></profile-image>
@endsection
