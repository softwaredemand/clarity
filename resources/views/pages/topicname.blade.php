@extends('layouts.app')

@section('content')
  <section>
    	<div class="bnr bnr-sub">
        	<div class="container">
            	<a href="">Back</a>
                <button>Filter</button>
            </div>
        </div>
        <div class="content-sec5s">
        	<div class="container">
                <div class="expertsec1s">
                	<h2>3957 experts in Entrepreneurship topic</h2>
                	<ul>
                        <li>
                            <div class="exprt1s">
                                <a href="#">
                                    <span><img src="images/thumb.jpg" alt=""></span>
                                </a>
                                    <div class="content-expert1s">
                                        <a href=""><h3>Get Top Tier PR  Media Exposure</h3></a>
                                        <h4><strong>Adrian Salamunovic</strong> <em></em> Los Angeles and Ottawa, Canada</h4>
                                        <p>Start-up Equity: I can help you determine exactly how much equity each person in your start up deserves. Inventor of the Grunt Fund</p>
                                        <h5>
                                        	<a href="#">Entrepreneurship</a>
                                            <a href="#">Start-ups</a>
                                            <a href="#">Marketing Communications</a>
                                            <a href="#">Public Speaking</a>
                                        </h5>
                                    </div>
                                </a>
                            </div>
                            <div class="exprt2s">
                                <h4>$16.67 <em>per minute</em></h4>
                                <a href="#">Request a Call</a>
                                <div class="small-stats1">
                                  <b><i class="fa fa-star-o" aria-hidden="true"></i> 4.9</b>
                                  <b><i class="fa fa-phone" aria-hidden="true"></i> 1.1k</b>
                                  <b><i class="fa fa-comment-o" aria-hidden="true"></i> 309</b>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="exprt1s">
                                <a href="#">
                                    <span><img src="images/thumb.jpg" alt=""></span>
                                </a>
                                    <div class="content-expert1s">
                                        <a href=""><h3>Get Top Tier PR  Media Exposure</h3></a>
                                        <h4><strong>Adrian Salamunovic</strong> <em></em> Los Angeles and Ottawa, Canada</h4>
                                        <p>Start-up Equity: I can help you determine exactly how much equity each person in your start up deserves. Inventor of the Grunt Fund</p>
                                        <h5>
                                        	<a href="#">Entrepreneurship</a>
                                            <a href="#">Start-ups</a>
                                            <a href="#">Marketing Communications</a>
                                            <a href="#">Public Speaking</a>
                                        </h5>
                                    </div>
                                </a>
                            </div>
                            <div class="exprt2s">
                                <h4>$16.67 <em>per minute</em></h4>
                                <a href="#">Request a Call</a>
                                <div class="starsec">
                                    <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><b>(192)</b>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <a href="#" class="ldbtn">Load more</a>
                </div>
            </div>
        </div>
    </section>
@endsection
