@extends('layouts.app')

@section('content')
@if(isset($expertise))
	<expertise-add
		:user="{{ json_encode($User) }}"
		:Category="{{ json_encode($Category) }}"
		:anexpert="{{ json_encode($anexpert) }}"
		:selected_expertise="{{ json_encode($expertise) }}"
		:expertise_images="{{ json_encode($expertise_images) }}"
		:selected_topics="{{ json_encode($topics) }}"
	></expertise-add>
	
@else
	<expertise-add
		:user="{{ json_encode($User) }}"
		:Category="{{ json_encode($Category) }}"
		:anexpert="{{ json_encode($anexpert) }}"
	></expertise-add>
@endif
@endsection