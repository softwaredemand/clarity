@extends('layouts.app')

@section('content')
   <section>
        <div class="bnr4">
            <h1>Make faster & better decisions to grow your business.</h1>
        </div>
        <div class="content-sec p-0">
            <div class="container">
                <div class="experts4s">
                    <ul>
                        <li>
                            <b><img src="images/img11.png" alt=""></b>
                            <span>
                                <h3>1. Schedule a Call</h3>
                                <p>Browse or search our community of experts to find the right one for you. Select 3 dates and times that work best for your schedule and specify a reason for the call. The expert has 72 hours to respond, or the request will expire.</p>
                            </span>
                        </li>
                        <li>
                            <span>
                                <h3>2. Get Confirmation</h3>
                                <p>You will receive a confirmation email with a conference line and access code provided. Block out your calendar and prepare some questions and topics to cover in the call. Feel free to invite up to 8 others from your team to dial in as well!</p>
                            </span>
                            <b><img src="images/img12.png" alt=""></b>
                        </li>
                        <li>
                            <b><img src="images/img13.png" alt=""></b>
                            <span>
                                <h3>3. Connect, Talk & Pay</h3>
                                <p>Call the conference line provided in your confirmation email. After the call, you’ll be charged the expert’s per-minute rate, and they get paid. You can then leave a rating and review for the expert.</p>
                            </span>
                        </li>
                    </ul>
                </div>
                <div class="tabwork">
                    <h3>Clarity Etiquette</h3>
                    <p>We ask that all members (including experts) review and follow the community guidelines outlined below. Our success is dependent on everyone participating with these principles in mind.</p>
                    <div class="tabswork">
                        <ul class="nav" role="tablist">
                            <li><a class="active" data-toggle="tab" href="#tab1">Be Open Minded</a></li>
                            <li><a data-toggle="tab" href="#tab2">Being on Time</a></li>
                            <li><a data-toggle="tab" href="#tab3">No Hawking</a></li>
                            <li><a data-toggle="tab" href="#tab4">Be Prepared</a></li>
                            <li><a data-toggle="tab" href="#tab5">Be Yourself</a></li>
                         </ul>
                        <div class="tab-content">
                        <div id="tab1" class="container tab-pane active">
                            <div class="inrwork">
                                <p>Open minds are strongly encouraged. There's no use allowing the conversation to turn abrasive. It'll get ya nowhere. If things start going off the tracks, it's sometimes best to wrap it up and move on.</p>
                            </div>
                        </div>
                        <div id="tab2" class="container tab-pane fade">
                            <div class="inrwork">
                                <p>Being on time shows respect and eagerness to meet the other person, which always sets things off on the right foot. We understand, things come up. Please do your best to re-schedule or cancel the call so as to free up time for the other person.</p>
                            </div>
                        </div>
                        <div id="tab3" class="container tab-pane fade">
                            <div class="inrwork">
                                <p>The reason you're here is to give or receive expert business advice, not to sell or be sold to. That being said, if, in the conversation, the other person says something like, "Wow, that's interesting - I'd love to be a customer" then it's fair game.</p>
                            </div>  
                        </div>
                        <div id="tab4" class="container tab-pane fade">
                            <div class="inrwork">
                                <p>We highly recommend you take 5-10 minutes before the call to prepare. Gather some questions or topics to cover in the call so you can quickly engage and make the most of the conversation. Preparation is key.</p>
                            </div>  
                        </div>
                        <div id="tab5" class="container tab-pane fade">
                            <div class="inrwork">
                                <p>Some people try and create business accounts on Clarity. That's not how we work. Our members want to know who YOU are, not your company. Real people and reputation is the key to making Clarity work.</p>
                            </div>  
                        </div>
                      </div>
                    </div>
                    <h6>Got questions? Visit our <a href="#">Help Center.</a></h6>
                </div>
            </div>
        </div>
    </section>
@endsection