@extends('layouts.app')

@section('stylesheet')
    <link rel="stylesheet" type="text/css" href="css/landing.css">
@endsection


@section('content')
    <div class="bnr-sec">
        @if(isset($landing_data['image']))
            <img src="{{ url('admin/storage/images/landing/'.$landing_data['image']) }}" alt="">
        @else
            <img src="images/bnr.jpg" alt="">
        @endif
            <div class="bnr-upr">
                <div class="container">
                    <div class="col-lg-7 col-md-8 col-sm-12 float-left">
                    @if(isset($landing_data))
                        <h1>{{$landing_data['title']}}</h1>
                    @else           
                        <h1>Startup Advice <span>from</span> World Class Experts</h1>
                    @endif
                        <div class="bnr-inr">
                            <h2>Verified Expert</h2>
                            <h3><a href="#">Kate Kendall <span></span><em></em></a></h3>
                            <h4>Founder and CEO of CloudPeeps. Created The Fetch.</h4>
                            <ul>
                                <li><a href="browse/Fundraising">Fundraising</a></li>
                                <li><a href="browse/Marketplaces">Marketplaces</a></li>
                                <li><a href="browse/Social">Social Media Marketing</a></li>
                                <li><a href="browse/Email">Email Marketing</a></li>
                                <li><a href="browse/Community">Community Building</a></li>
                            </ul>
                            <div class="btnsec">
                                <a class="btn1" href="account">Find Your Expert</a>
                                <a href="#">Learn More</a>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <div class="work-sec">
            <div class="container">
                <h2>{{__('how_it_works')}}</h2>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 float-left">
                        <h3>Step-by-Step</h3>
                        <div class="firstsec">
                            <ul>
                                <li><em><b>1</b></em>
                                    <span>
                                        <h4>{{__('find_an_expert')}}</h4>
                                        <p>{{__('desc1')}}</p>
                                    </span>
                                </li>
                                <li><em><b>2</b></em>
                                    <span>
                                        <h4>{{__('request_a_call')}}</h4>
                                        <p>{{__('desc2')}}</p>
                                    </span>
                                </li>
                                <li><em><b>3</b></em>
                                    <span>
                                        <h4>{{__('connect_properly')}}</h4>
                                        <p>{{__('desc3')}}</p>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 float-left">
                        <h3>{{__('browse_topics')}}</h3>
                        <div class="form-inr">
                            <form>
                                <input type="text" placeholder="Search topic">
                            </form>
                            <ul>
                                <li><a href="topics/entrepreneurs">Entrepreneurship</a></li>
                                <li><a href="topics/entrepreneurs">Business Development</a></li>
                                <li><a href="topics/entrepreneurs">Marketing Strategy</a></li>
                                <li><a href="topics/entrepreneurs">Social Media</a></li>
                                <li><a href="topics/entrepreneurs">Business Strategy</a></li>
                                
                                <li><a href="topics/entrepreneurs">Online Marketing</a></li>
                                <li><a href="topics/entrepreneurs">Social Media Marketing</a></li>
                                <li><a href="topics/entrepreneurs">Marketing</a></li>
                                <li><a href="topics/entrepreneurs">Digital Marketing</a></li>
                                <li><a href="topics/entrepreneurs">Strategic Planning</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 float-left">
                        <h3>{{__('get_answers')}}</h3>
                        <div class="thrdsec">
                            <a href="answer-details">{{__('desc4')}}</a>
                        </div>
                        <div class="secinr">
                            <a href="#">4 Answers from</a>
                            <ul>
                                <li><a href="#"><img src="images/thumb.jpg" alt=""></a></li>
                                <li><a href="#"><img src="images/thumb.jpg" alt=""></a></li>
                                <li><a href="#"><img src="images/thumb.jpg" alt=""></a></li>
                                <li><a href="#"><img src="images/thumb.jpg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 float-left btnwork">
                        <a href="account">{{__('find_an_expert')}}</a>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 float-left btnwork">
                        <a href="topics">{{__('view_all_topics')}}</a>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 float-left btnwork">
                        <a href="question/new">{{__('ask_a_question')}}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="expertsec">
            <div class="container bgsec">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 float-left">
                        <h2>{{__('are_you_expert')}}</h2>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 float-left">
                        <p>{{__('desc4')}}</p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 float-left">
                        <a href="account">{{__('become_an_expert')}}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 float-left">
                    <div class="clocksec">
                        <div class="clock">
                          <div class="top"></div>
                          <div class="right"></div>
                          <div class="bottom"></div>
                          <div class="left"></div>
                          <div class="center"></div>
                          <div class="hour"></div>
                          <div class="minute"></div>
                          <div class="second"></div>
                        </div>
                        <h3>EVERY</h3>
                        <h4>90<span>second</span></h4>
                        <h5>a startup is on the phone with a Clarity Expert</h5>
                        <ul>
                          <li><span class="number">73k+</span> <span class="content">Completed calls with Experts</span></li>
                          <li><span class="number">98%</span> <span class="content">Positive Clarity Expert calls</span></li>
                          <li><span class="number">4.8/5</span> <span class="content">Average Clarity Expert star rating</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 float-left">
                    <div class="testisec">
                        <p>"We really needed help with our messaging and brand story. Clarity helped us get there:
                        <strong>at TechCrunch Disrupt, we literally had lines of people wanting to talk to us and watch our demo.</strong>"</p>
                        <span><img src="images/img1.png" alt=""></span>
                        <h3>M. Geneles & A. Gopshtein</h3>
                        <h4>Founders of Pitchbox</h4>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="ftrmdl">
            <a href="#">Clarity</a>
        </div>
        <div class="ftr">
            <div class="container">
                <ul class="ftrsec">
                    <li>
                        <h3><a href="">{{__('about')}}</a></h3>
                        <a href="how-it-work">{{__('how_it_works')}}</a>
                        <a href="customers">{{__('success_stories')}}</a>
                    </li>
                    <li>
                        <h3><a href="#">{{__('experts')}}</a></h3>
                        <a href="account">{{__('become_an_expert')}}</a>
                        <a href="browse">{{__('find_an_expert')}}</a>
                    </li>
                    <li>
                        <h3><a href="#">{{__('answers')}}</a></h3>
                        <a href="question/new">{{__('ask_a_question')}}</a>
                        <a href="questions">{{__('recent_answers')}}</a>
                    </li>
                    <li>
                        <h3><a href="#">{{__('support')}}</a></h3>
                        <a href="help">{{__('help')}}</a>
                        <a href="terms">{{__('terms_of_services')}}</a>
                    </li>
                    <li>
                        <h3>{{__('follow')}}</h3>
                        <h4>
                            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </h4>
                    </li>
                </ul>
            </div>
        </div>
        <div class="ftrbtm">
            <div class="container">
                <h3>the <b>startups.com</b> platform</h3>
                <ul>
                    <li>
                        <a href="#">
                            <img src="images/01.png" alt="">
                            <h5>Startups Education</h5>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/01.png" alt="">
                            <h5>Startup Planning</h5>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/01.png" alt="">
                            <h5>Access Mentors</h5>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/01.png" alt="">
                            <h5>Secure Funding</h5>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/01.png" alt="">
                            <h5>Reach Customers</h5>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/01.png" alt="">
                            <h5>Virtual Assistants</h5>
                        </a>
                    </li>
                </ul>
                <h4>Copyright &copy; 2021 Startups.com. All rights reserved.</h4>
            </div>
        </div>
    </footer>
@endsection
