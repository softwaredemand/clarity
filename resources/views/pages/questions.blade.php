@extends('layouts.app')

@section('content')
	<questions :questions="{{$questions}}" :question_topics="{{$question_topics}}"></questions>
@endsection