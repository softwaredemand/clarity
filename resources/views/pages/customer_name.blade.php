@extends('layouts.app')

@section('content')
   <section>
    	<div class="bnr2 p-0 border-0">
        	<div class="container">
            	<div class="toptab7s">
                	<div class="current">
                        <h3><a href="#">Success Stories</a> › Esther Kuperman</h3>
                        <h4>Next Success Story › <a href="#">Cheryl Yeoh</a></h4>
                      </div>
                 </div>
            </div>
        </div>
        <div class="costomersec custdetil">
        	<div class="container">
            	<span><img src="images/img1.png" alt=""></span>
                <b><h2>"I was looking for deep knowledge on marketing mobile apps. I’ve always known that as a marketer, you need to run tests to determine what works and what doesn’t. I don’t have that kind of time or money, so I opted for a smarter approach — to consult experts who’ve been there before." — Esther Kuperman</h2>
                <p>An established marketing leader who’s building Sociidot — a mobile app that helps users pursue their hopes, goals, and dreams by turning their journeys into stories.</p>
                </b>
                <div class="detailsec">
                	<ul>
                    	<li>
                        	<h3>Quick Facts:</h3>
                          	<dl>
                                <dt>Industry:</dt>
                                <dd>Mobile, Marketing</dd>
                                <dt>Location:</dt>
                                <dd>New York City</dd>
                                <dt>Clarity Member Since:</dt>
                                <dd>December 2012</dd>
                          	</dl>
                       </li>
                       <li>
                        	<h3>Clarity Stats:</h3>
                          	<dl>
                                <dt>Industry:</dt>
                                <dd>Mobile, Marketing</dd>
                                <dt>Location:</dt>
                                <dd>New York City</dd>
                                <dt>Clarity Member Since:</dt>
                                <dd>December 2012</dd>
                          	</dl>
                       </li>
                       <li>
                        	<h3>Core Results:</h3>
                          	<dl>
                                <dt>Industry:</dt>
                                <dd>Mobile, Marketing</dd>
                                <dt>Location:</dt>
                                <dd>New York City</dd>
                                <dt>Clarity Member Since:</dt>
                                <dd>December 2012</dd>
                          	</dl>
                       </li>
                       <li>
                        	<h3>ROI:</h3>
                          	<dl>
                                <dt>Industry:</dt>
                                <dd>Mobile, Marketing</dd>
                                <dt>Location:</dt>
                                <dd>New York City</dd>
                                <dt>Clarity Member Since:</dt>
                                <dd>December 2012</dd>
                          	</dl>
                       </li>
                    </ul>
                </div>
                <h3>Make faster & better decisions to grow your business. <a href="#">Start using Clarity</a></h3>
            </div>
        </div>
        <div class="custdetil2">
        	<div class="container">
            	<ul>
                	<li>
                    	<h3><img src="images/imgico1.png" alt="">Her Story</h3>
                        <p>Esther always knew that she wanted to be an entrepreneur but always believed that she needed to ‘pay her dues’ and ‘work her way up.’ After years as a marketing and brand experience designer, she felt a strong passion to build her own products. After trying out some ideas, she figured out her true calling — an app called Sociidot.</p>
                        <div class="qsmall">
                          <span><img src="images/img1.png" alt=""></span>
                          <div class="bubble1">
                            <p>"I ask people how many hours a day they spend doing what they love. The answer? Most people say 10 minutes. Others — one or two hours. That’s why I created Sociidot, a resource that helps people move towards their passions and goals. "</p>
                          </div>
                        </div>
                    </li>
                    <li>
                    	<h3><img src="images/imgico2.png" alt="">Life Before Clarity</h3>
                        <p>Esther always knew that she wanted to be an entrepreneur but always believed that she needed to ‘pay her dues’ and ‘work her way up.’ After years as a marketing and brand experience designer, she felt a strong passion to build her own products. After trying out some ideas, she figured out her true calling — an app called Sociidot.</p>
                        <div class="qsmall">
                          <span><img src="images/img1.png" alt=""></span>
                          <div class="bubble1">
                            <p>"I ask people how many hours a day they spend doing what they love. The answer? Most people say 10 minutes. Others — one or two hours. That’s why I created Sociidot, a resource that helps people move towards their passions and goals. "</p>
                          </div>
                        </div>
                    </li>
                    <li>
                    	<h3><img src="images/imgico3.png" alt="">Clarity Empowers Her with One-of-a-Kind, In-Depth Knowledge</h3>
                        <p>Esther always knew that she wanted to be an entrepreneur but always believed that she needed to ‘pay her dues’ and ‘work her way up.’ After years as a marketing and brand experience designer, she felt a strong passion to build her own products. After trying out some ideas, she figured out her true calling — an app called Sociidot.</p>
                        <div class="qsmall">
                          <span><img src="images/img1.png" alt=""></span>
                          <div class="bubble1">
                            <p>"I ask people how many hours a day they spend doing what they love. The answer? Most people say 10 minutes. Others — one or two hours. That’s why I created Sociidot, a resource that helps people move towards their passions and goals. "</p>
                          </div>
                        </div>
                    </li>
                    <li>
                    	<h3><img src="images/imgico4.png" alt="">Key Calls</h3>
                        <p>Several key moments helped push Esther forward:</p>
                        <div class="qsmall2">
                          <span><img src="images/img1.png" alt=""></span>
                          <b>
                          	<h5><a href="#">Chris Bruce</a><em>— Entrepreneur, Developer</em></h5>
                          	<h6>Process of building an app</h6>
                          </b>
                        </div>
                        <div class="qsmall2">
                          <span><img src="images/img1.png" alt=""></span>
                          <b>
                          	<h5><a href="#">Chris Bruce</a><em>— Entrepreneur, Developer</em></h5>
                          	<h6>Process of building an app</h6>
                          </b>
                        </div>
                        <div class="qsmall2">
                          <span><img src="images/img1.png" alt=""></span>
                          <b>
                          	<h5><a href="#">Chris Bruce</a><em>— Entrepreneur, Developer</em></h5>
                          	<h6>Process of building an app</h6>
                          </b>
                        </div>
                    </li>
                </ul>
                <strong>Need Business Help? <a href="#">Start using Clarity</a></strong>
                <div class="current">
                    <h3><a href="#">Success Stories</a> › Esther Kuperman</h3>
                    <h4>Next Success Story › <a href="#">Cheryl Yeoh</a></h4>
                  </div>
            </div>
        </div>
        <div class="contbtm">
        	<div class="container">
            	<h3>Powell, OH</h3>
                <p>1322 Manning Parkway <br>Powell, OH 43065</p>
                <h3>Help</h3>
                <p>Need Help? Feel free to email us your question directly or setup an appointment for phone support by emailing <a href="#" >support@clarity.fm</a>. You can also checkout our <a href="#">Help Center</a> to see if we've already answered your question.</p>
                <h3>Press Inquiries</h3>
                <p>If you’re working on a piece about Clarity, please feel free to <a href="#">email us</a> directly. We can provide background information about the company and availability for our team members regarding speaking opportunities.</p>
                <p><a href="#">Download Media Kit</a></p>
            </div>
        </div>
    </section>
@endsection
