@extends('layouts.app')

@section('content')
 <section>
    	<div class="bnr bnr-sub">
        	<div class="container">
            	<a href="setting">Back</a>
            </div>
        </div>
        <div class="content-sec1">
        	<div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 float-left">
                    	<h1>Promotions</h1>
                        <form>
                        <ul>
                            <li><label>Enter a code</label><input type="text" placeholder="Promotion code"></li>
                            <li><input type="submit" value="Apply"></li>
                        </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
