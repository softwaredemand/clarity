@extends('layouts.app')

@section('content')
    <section>
    <div class="bnr bnr-sub">
        <div class="container">
            <a href="setting">Back</a>
        </div>
    </div>
    @if(isset($card))
        <div class="content-sec7s m-0">
            <div class="container">
                <h6>Your Card Details</h6>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 float-left" style="background: #f7f6f1">
                        <p>Exp Year : <b>{{$card['exp_year']}}</b></p>
                        <p>Exp Month : <b>{{$card['exp_month']}}</b></p>
                        <p>Last 4 digit of card: <b>{{$card['last4']}}</b></p>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="content-sec7s m-0">
        	<div class="container">
            	<h6>Please provide your payment info.</h6>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 float-left">
                    <div class="alert alert-danger text-center" id="err" style="display:none">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    </div>
                        <form action="{{ route('stripe.post') }}" 
                            method="post" 
                            class="require-validation"
                            data-cc-on-file="false"
                            data-stripe-publishable-key="pk_test_51J9dJDBmJaUTgpglucKrG121YTMb2zqwrKXN5AD9pKDlTT3JmUvogqFrMsoWnAPCkwe73BI2XTO1s4aOUm8fE3c700IrDTd2Iw"
                            id="payment-form">
                            @csrf
                            <h2>Payment Details <img src="images/img19.png" alt=""></h2>
                            <ul class="secthrd">
                            	<li><h3><strong>To update, please supply a new card below.</strong></h3></li>
                            	<li><label>Credit Card</label><input class="cc-month card-number" type="text" placeholder="Card number"><input class="cc-year card-cvc" type="text" placeholder="CVC"></li>
                                <li><label>Expiration</label><select class="cc-month card-expiry-month">
                                <option value="">Month</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                </select><select class="cc-year card-expiry-year">
                                <option value="">Year</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                                <option value="2028">2028</option>
                                <option value="2029">2029</option>
                                <option value="2030">2030</option>
                                <option value="2031">2031</option>
                                <option value="2032">2032</option>
                                <option value="2033">2033</option>
                                <option value="2034">2034</option>
                                <option value="2035">2035</option>
                                <option value="2036">2036</option>
                                <option value="2037">2037</option>
                                <option value="2038">2038</option>
                                <option value="2039">2039</option>
                                <option value="2040">2040</option>
                                </select></li>
                                <li><h3>Your information is kept 100% private!</h3></li>
                            </ul>
                            <ul class="secfrth">
                            	<li><input type="submit" value="Save"></li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
@endsection
@section('js')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
    <script type="text/javascript">
    $(function() {
        
        var $form = $(".require-validation");
    
        $('form.require-validation').bind('submit', function(e) {
            var $form         = $(".require-validation"),
            inputSelector = ['input[type=email]', 'input[type=password]',
                            'input[type=text]', 'input[type=file]',
                            'textarea'].join(', '),
            $inputs       = $form.find('.required').find(inputSelector),
            $errorMessage = $form.find('div.error'),
            valid         = true;
            $errorMessage.addClass('hide');
    
            $('.has-error').removeClass('has-error');
            $inputs.each(function(i, el) {
            var $input = $(el);
            if ($input.val() === '') {
                $input.parent().addClass('has-error');
                $errorMessage.removeClass('hide');
                e.preventDefault();
            }
            });
    
            if (!$form.data('cc-on-file')) {
            e.preventDefault();
            Stripe.setPublishableKey($form.data('stripe-publishable-key'));
            Stripe.createToken({
                number: $('.card-number').val(),
                cvc: $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler);
            }
    
    });
    
    function stripeResponseHandler(status, response) {
        if (response.error) {
            $('#err').css('display','block').append(`<p>${response.error.message}</p>`);
        } else {
            /* token contains id, last4, and card type */
            var token = response['id'];
            
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }
    
    });
</script>
@endsection