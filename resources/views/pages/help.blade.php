@extends('layouts.app')

@section('content')
  <section>
    	<div class="bnr2">
        	<div class="container">
            	<div class="toptab3s">
                	<h1>Help Center</h1>
                 </div>
            </div>
        </div>
        <div class="content-sec">
        	<div class="container">
                <div class="experts1s">
                	<div class="row">
                    	<div class="col-lg-4 col-md-6 col-sm-12 float-left">
                        	<h3>Getting Started on Clarity</h3>
                            <ul>
                            	<li><a href="help/article/6/how-it-works">What is Clarity?</a></li>
                                <li><a href="help/article/6/how-it-works">How does Clarity work?</a></li>
                                <li><a href="help/article/6/how-it-works">How do I sign up?</a></li>
                                <li><a href="help/article/6/how-it-works">Are there guidelines or rules of conduct on Clarity?</a></li>
                                <li><a href="help/article/6/how-it-works">Is Clarity International?</a></li>
                            </ul>
                            <div class="countsec">
                            	<a href="help/article/6/how-it-works">View All</a>
                                <b>5 articles</b>
                            </div>
                            <h3>Getting Started on Clarity</h3>
                            <ul>
                            	<li><a href="help/article/6/how-it-works">What is Clarity?</a></li>
                                <li><a href="help/article/6/how-it-works">How does Clarity work?</a></li>
                                <li><a href="help/article/6/how-it-works">How do I sign up?</a></li>
                                <li><a href="help/article/6/how-it-works">Are there guidelines or rules of conduct on Clarity?</a></li>
                                <li><a href="help/article/6/how-it-works">Is Clarity International?</a></li>
                            </ul>
                            <div class="countsec">
                            	<a href="help/article/6/how-it-works">View All</a>
                                <b>5 articles</b>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 float-left">
                        	<h3>Your Clarity Account</h3>
                            <ul>
                            	<li><a href="help/article/6/how-it-works">How do I edit my profile?</a></li>
                                <li><a href="help/article/6/how-it-works">How can I change my phone number and manage notifications?</a></li>
                                <li><a href="help/article/6/how-it-works">Why didn't I receive my email or SMS notification?</a></li>
                                <li><a href="help/article/6/how-it-works">How do I delete my account?</a></li>
                            </ul>
                            <div class="countsec">
                            	<a href="help/article/6/how-it-works">View All</a>
                                <b>7 articles</b>
                            </div>
                            <h3>Your Clarity Account</h3>
                            <ul>
                            	<li><a href="help/article/6/how-it-works">How do I edit my profile?</a></li>
                                <li><a href="help/article/6/how-it-works">How can I change my phone number and manage notifications?</a></li>
                                <li><a href="help/article/6/how-it-works">Why didn't I receive my email or SMS notification?</a></li>
                                <li><a href="help/article/6/how-it-works">How do I delete my account?</a></li>
                            </ul>
                            <div class="countsec">
                            	<a href="help/article/6/how-it-works">View All</a>
                                <b>7 articles</b>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 float-left ansform">
                        	<form>
                                <input type="text" placeholder="Search">
                                <input type="submit" value="&#xf002" class="fa">
                            </form>
                            <div class="btmright">
                            	<h3>Can't find what you're looking for?</h3>
                                <p><a href="help/article/6/how-it-works">Send us an email.</a> Phone support is available by appointment only. Please email us your name, phone number, problem/issue and availability Mon - Fri 9am - 5pm EDT.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection