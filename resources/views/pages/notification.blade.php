@extends('layouts.app')

@section('content')
  <section>
    	<div class="bnr bnr-sub">
        	<div class="container">
            	<a href="setting">Back</a>
            </div>
        </div>
        <div class="content-sec">
        	<div class="container">
            	<h1>Notifications</h1>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 float-left noticform">
                        <form>
                        	<h2>Emails</h2>
                        	<ul>
                                <li><div class="inrline"><h5>Call Requests</h5><label class="switch"><input type="checkbox"><span class="slider round"></span></label></div></li>
                                <li><div class="inrline"><h5>Call Reminder</h5><label class="switch"><input type="checkbox"><span class="slider round"></span></label></div></li>
                                <li><div class="inrline"><h5>Daily Digest</h5><label class="switch"><input type="checkbox"><span class="slider round"></span></label></div></li>
                                <li><div class="inrline"><h5>Recommendations</h5><label class="switch"><input type="checkbox"><span class="slider round"></span></label></div></li>
                                <li><div class="inrline"><h5>Clarity Updates</h5><label class="switch"><input type="checkbox"><span class="slider round"></span></label></div></li>
                                <li><div class="inrline"><h5>Favorites</h5><label class="switch"><input type="checkbox"><span class="slider round"></span></label></div></li>
                                <li><div class="inrline"><h5>Clarity Questions</h5><label class="switch"><input type="checkbox"><span class="slider round"></span></label></div></li>
                            </ul>
                            <h3>Your email is <b>ayazaries1989@gmail.com</b>. (<a href="#">edit</a>)</h3>
                            <h2>SMS</h2>
                        	<ul>
                                <li><div class="inrline"><h5>Call Management</h5><label class="switch"><input type="checkbox"><span class="slider round"></span></label></div></li>
                                <li><div class="inrline"><h5>Call Reminder</h5><label class="switch"><input type="checkbox"><span class="slider round"></span></label></div></li>
                            </ul>
                            <h3>Your phone number is <b>92-3001234567</b> (<a href="#">edit</a>)</h3>
                        </form>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 float-left">
                    </div>
                </div>
            </div>
        </div>
    </section>	
@endsection