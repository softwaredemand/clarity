@extends('layouts.app')

@section('content')
    <answer-details :question="{{$question}}" :questions="{{$questions}}" :question_topics="{{$question_topics}}"></answer-details>
@endsection