@extends('layouts.app')

@section('content')
<user-profile-view :user="{{ json_encode($user) }}" :expertise_fvrt_user_ids="{{$expertise_fvrt_user_ids}}" :related_experts="{{$related_experts}}"></user-profile-view>
@endsection
