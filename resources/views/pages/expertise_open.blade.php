@extends('layouts.app')

@section('content')
	<expertise-open :user="{{$user}}" 
		:expertise="{{$expertise}}" 
		:category="{{$category}}" 
		:expertise_images={{$expertise_images}} 
		:expertise_topics="{{$expertise_topics}}" 
		:expertise_fvrt_user_ids="{{$expertise_fvrt_user_ids}}">
	</expertise-open>
@endsection