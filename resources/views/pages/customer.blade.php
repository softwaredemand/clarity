@extends('layouts.app')

@section('content')
	 <section>
    	<div class="bnr2 bgcust">
        	<div class="container">
            	<div class="toptab3s">
                	<h1>Success Stories — Learn why our members love using Clarity</h1>
                 </div>
            </div>
        </div>
        <div class="costomersec">
        	<div class="container">
            	<h3>Make faster & better decisions to grow your business. <a href="signup">Sign Up for Free</a></h3>
                <div class="row">
                    <ul>
                        <li>
                            <div class="custsec">
                                <div class="share-button"><a href="#"><i class="fa fa-share-square-o" aria-hidden="true"></i></a></div>
                                <div class="topcust">
                                    <span><img src="images/img1.png" alt=""></span>
                                    <b>
                                        <h3>M. Geneles & A. Gopshtein</h3>
                                        <h4>Founders of Pitchbox</h4>
                                    </b>
                                </div>
                                <div class="mdlcust">
                                <p>Technical co-founders who are building a marketing prospecting and outreach automation platform for digital marketing agencies.</p>
                                <a href="customers/harimaqsood">Read Success Story</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="custsec">
                                <div class="share-button"><a href="#"><i class="fa fa-share-square-o" aria-hidden="true"></i></a></div>
                                <div class="topcust">
                                    <span><img src="images/img1.png" alt=""></span>
                                    <b>
                                        <h3>M. Geneles & A. Gopshtein</h3>
                                        <h4>Founders of Pitchbox</h4>
                                    </b>
                                </div>
                                <div class="mdlcust">
                                <p>Technical co-founders who are building a marketing prospecting and outreach automation platform for digital marketing agencies.</p>
                                <a href="customers/harimaqsood">Read Success Story</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="custsec">
                                <div class="share-button"><a href="#"><i class="fa fa-share-square-o" aria-hidden="true"></i></a></div>
                                <div class="topcust">
                                    <span><img src="images/img1.png" alt=""></span>
                                    <b>
                                        <h3>M. Geneles & A. Gopshtein</h3>
                                        <h4>Founders of Pitchbox</h4>
                                    </b>
                                </div>
                                <div class="mdlcust">
                                <p>Technical co-founders who are building a marketing prospecting and outreach automation platform for digital marketing agencies.</p>
                                <a href="customers/harimaqsood">Read Success Story</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="custrevw">
                    <h6>Customer Quotes</h6>
                    <p>Read what people say about us.</p>
                    <ul>
                    	<li>
                        	<div class="bubble">
                            	<p>If you're as crazy busy as I am, you will love that Clarity handles EVERYTHING — scheduling, billing, setup of the conference call, and even "I want to donate the proceeds of the call to charity"!  Simple, easy, fast.</p>
                            </div>
                            <div class="avatar">
                            	<img src="images/img1.png" alt="">
                                <h5>Jonathan Feldman <b>— Contributing Editor @ Informationweek.com</b></h5>
                            </div>
                        </li>
                        <li>
                        	<div class="bubble">
                            	<p>If you're as crazy busy as I am, you will love that Clarity handles EVERYTHING — scheduling, billing, setup of the conference call, and even "I want to donate the proceeds of the call to charity"!  Simple, easy, fast.</p>
                            </div>
                            <div class="avatar">
                            	<img src="images/img1.png" alt="">
                                <h5>Jonathan Feldman <b>— Contributing Editor @ Informationweek.com</b></h5>
                            </div>
                        </li>
                    </ul>
                </div>
                <h4>Make faster & better decisions to grow your business. <a href="#">Sign Up for Free</a></h4>
            </div>
        </div>
        <div class="contbtm">
        	<div class="container">
            	<h3>Powell, OH</h3>
                <p>1322 Manning Parkway <br>Powell, OH 43065</p>
                <h3>Help</h3>
                <p>Need Help? Feel free to email us your question directly or setup an appointment for phone support by emailing <a href="#" >support@clarity.fm</a>. You can also checkout our <a href="#">Help Center</a> to see if we've already answered your question.</p>
                <h3>Press Inquiries</h3>
                <p>If you’re working on a piece about Clarity, please feel free to <a href="#">email us</a> directly. We can provide background information about the company and availability for our team members regarding speaking opportunities.</p>
                <p><a href="#">Download Media Kit</a></p>
            </div>
        </div>
    </section>
@endsection