@extends('layouts.app')

@section('content')
<topic :userId="{{ json_encode($userId) }}"></topic>
@endsection
