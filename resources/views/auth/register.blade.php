@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <link rel="stylesheet" type="text/css" href="css/util.css">
@endsection
@section('content')
<div class="limiter">
    <div class="container-login100" style="background-image: url('images/bg-01.jpg');">
        <div class="wrap-login100 p-l-30 p-r-30 p-t-45 p-b-30" style="margin-top:90px;">
            <form class="login100-form validate-form" method="POST" action="{{ route('register') }}">
                @csrf
                <span class="login100-form-title">
                    Sing Up for Clarity
                </span>

                <p style="font-size: large;margin-top: 40px;" class="text-center">Join Clarity now — it's free!</p>

                <div class="wrap-input100 validate-input m-b-23 mt-4">
                    <input class="input100 @error('name') is-invalid @enderror" type="text" placeholder="Name" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror    
                </div>

                <div class="wrap-input100 validate-input">
                    <input class="input100 @error('email') is-invalid @enderror" type="text" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror    
                </div>

                <div class="wrap-input100 validate-input mt-4">
                    <input class="input100 @error('password') is-invalid @enderror" 
                        type="password" placeholder="password"
                        name="password" required autocomplete="new-password"
                        id="password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="wrap-input100 validate-input mt-4">
                    <input class="input100" 
                        type="password" 
                        placeholder="Confirm password" 
                        name="password_confirmation" required autocomplete="new-password">
                </div>
                <br>
                <button type="submit" class="btn btn-success btn-block mt-4">{{ __('CONTINUE') }}</button>

                <div class="txt1 text-center p-t-20 p-b-15">
                    <a href="/login"><b>Already a Clarity member? Log In</b></a>
                </div>
                
                <hr>
                <p class="text-center" style="font-size: 13px;">By signing up, you agree to our <a href="#">Terms of Use</a> and <a href="#"> Privacy Policy</a>, and <br> you confirm that you're 18 years old or over.</p>
                
            </form>
        </div>
    </div>
</div>
@endsection
