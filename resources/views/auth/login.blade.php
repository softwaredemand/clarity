@extends('layouts.app')

@section('stylesheet')
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <link rel="stylesheet" type="text/css" href="css/util.css">
@endsection

@section('content')
<div class="limiter mt-5">
    <div class="container-login100" style="background-image: url('images/bg-01.jpg');">
        <div class="wrap-login100 p-l-30 p-r-30 p-t-45 p-b-30">
            <form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
                @csrf
                <span class="login100-form-title">
                    Login 
                </span>

                <div class="wrap-input100 validate-input m-b-23 mt-4">
                    <input class="input100 @error('email') is-invalid @enderror" type="text" id="email" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="wrap-input100 validate-input">	
                    <input id="password" class="input100 @error('password') is-invalid @enderror" type="password" name="password" placeholder="password" required autocomplete="current-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <br>
                <button type="submit" class="btn btn-success btn-block mt-4">{{ __('Login') }}</button>
                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif

                <div class="txt1 text-center p-t-20 p-b-15">
                    <a href="register"><b>Don't have a Clarity account yet? Sign Up</b></a>
                </div>
                <hr>
                <p class="text-center">Log in using <a href="{{ url('auth/facebook') }}">Facebook</a> <br><a href="#">Forgot password?</a></p>
                <hr>
                <p class="text-center" style="font-size: 13px;">By signing up, you agree to our <a href="#">Terms of Use</a> and <a href="#"> Privacy Policy</a>, and <br> you confirm that you're 18 years old or over.</p>
                
            </form>
        </div>
    </div>
</div>
@endsection
