/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');

 window.Vue = require('vue');
 
 
 import VeeValidate from 'vee-validate';
 import Dropdown from 'vue-simple-search-dropdown';
 import VueToast from 'vue-toast-notification';
 import 'vue-toast-notification/dist/theme-sugar.css';
 import { Lang } from 'laravel-vue-lang';
 window.human = require('human-time');
 
 Vue.use(Lang, {
	locale: window.locale,
	fallback: 'en'
});

 Vue.use(Dropdown);
 Vue.use(VueToast);

 Vue.use(VeeValidate, {
    events: 'input|change|blur',
 });
     
 /**
  * The following block of code may be used to automatically register your
  * Vue components. It will recursively scan this directory for the Vue
  * components and automatically register them with their "basename".
  *
  * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
  */
 
 // const files = require.context('./', true, /\.vue$/i)
 // files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
 
 // Vue.component('login', require('./components/Auth/login.vue').default);
 // Vue.component('register', require('./components/Auth/register.vue').default);
 Vue.component('account', require('./components/account.vue').default);
 Vue.component('profile', require('./components/Auth/profile.vue').default);
 Vue.component('info', require('./components/info.vue').default);
 Vue.component('user-profile-view', require('./components/user-profile-view.vue').default);
 Vue.component('profile-image', require('./components/Auth/profileImage.vue').default);
 Vue.component('topic', require('./components/topic.vue').default);
 Vue.component('verifications', require('./components/verifications.vue').default);
 Vue.component('landing', require('./components/landing.vue').default);
 Vue.component('expertise', require('./components/expertise.vue').default);
 Vue.component('expertise-info', require('./components/expertise-info.vue').default);
 Vue.component('expertise-video', require('./components/expertise-video.vue').default);
 Vue.component('expertise-add', require('./components/expertise-add.vue').default);
 Vue.component('browse', require('./components/browse.vue').default);
 Vue.component('browse-category', require('./components/browse-category.vue').default);
 Vue.component('favorites', require('./components/favorites.vue').default);
 Vue.component('inbox', require('./components/conversations/inbox.vue').default);
 Vue.component('create', require('./components/conversations/create.vue').default);
 Vue.component('questions', require('./components/questions.vue').default);
 Vue.component('expertise-open', require('./components/expertise-open.vue').default);
 Vue.component('topics', require('./components/topics.vue').default);
 Vue.component('calls', require('./components/calls.vue').default);
 Vue.component('navbar-menu', require('./components/navbar.vue').default);
 Vue.component('update-password', require('./components/Auth/updatePassword.vue').default);
 Vue.component('ask-question', require('./components/askQuestion.vue').default);
 Vue.component('popular-answers', require('./components/popularAnswers.vue').default);
 Vue.component('search', require('./components/search.vue').default);
 Vue.component('create-answer', require('./components/quoteAnswer.vue').default);
 Vue.component('answer-details', require('./components/answerDetails.vue').default);
 Vue.component('recent-activities', require('./components/recentActivities.vue').default);
 Vue.component('setting', require('./components/setting.vue').default);

 /**	
  * Next, we will create a fresh Vue application instance and attach it to
  * the page. Then, you may begin adding components to this application
  * or customize the JavaScript scaffolding to fit your unique needs.
  */

const app = new Vue({
    el: '#app',
});