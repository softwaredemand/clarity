<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    // php artisan make:migration create_links_table
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('username')->nullable();
            $table->string('short_bio')->nullable();
            $table->string('mini_resume')->nullable();
            $table->string('cell_phone')->nullable();
            $table->string('location')->nullable();
            $table->string('time_zone')->nullable();
            $table->string('cell_code')->nullable();
            $table->string('customer_id')->nullable();
            $table->string('stripe_card_info')->nullable();
            $table->string('profile_image')->nullable();
            $table->boolean('is_expert')->default(false)->index();
            $table->string('hourly_rate')->nullable();
            $table->string('charity')->nullable();
            $table->string('is_deleted')->default(false)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
