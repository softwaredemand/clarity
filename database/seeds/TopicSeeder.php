<?php

use Illuminate\Database\Seeder;
use App\Topic;

class TopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Topic::truncate();
        Topic::create([
            'key' => Str::slug('Web Design'),
            'name' => 'Web Design',
        ]);

        Topic::create([
            'key' => Str::slug('Web Development'),
            'name' => 'Web Development',
        ]);
        
        Topic::create([
            'key' => Str::slug('WordPress'),
            'name' => 'WordPress',
        ]);

        Topic::create([
            'key' => Str::slug('Software Development'),
            'name' => 'Software Development',
        ]);

        Topic::create([
            'key' => Str::slug('Social Media'),
            'name' => 'Social Media',
        ]);

        Topic::create([
            'key' => Str::slug('Digital Marketing'),
            'name' => 'Digital Marketing',
        ]);

        Topic::create([
            'key' => Str::slug('Public Speaking'),
            'name' => 'Public Speaking',
        ]);

        Topic::create([
            'key' => Str::slug('Businees'),
            'name' => 'Businees',
        ]);
    }
}
