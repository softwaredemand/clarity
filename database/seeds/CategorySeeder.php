<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    // php artisan make:seeder UserSeeder
    // php artisan db:seed
    public function run()
    {
        Category::truncate();
    	Category::create([
            'key' => Str::slug('Business'),
            'name' => 'Business'
        ]);

        Category::create([
            'key' => Str::slug('Sales and Marketing'),
            'name' => 'Sales & Marketing'
        ]);

        Category::create([
            'key' => Str::slug('Funding'),
            'name' => 'Funding'
        ]);

        Category::create([
            'key' => Str::slug('Product and Design'),
            'name' => 'Product & Design'
        ]);

        Category::create([
            'key' => Str::slug('Technology'),
            'name' => 'Technology'
        ]);

        Category::create([
            'key' => Str::slug('Skills and Management'),
            'name' => 'Skills & Management'
        ]);


        Category::create([
            'key' => Str::slug('Industries'),
            'name' => 'Industries'
        ]);

        Category::create([
            'key' => Str::slug('other'),
            'name' => 'other'
        ]);
    }
}
